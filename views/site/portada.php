<?php 
use app\helpers\String;
use app\helpers\Fechas;
?>    
<?php if ($this->beginCache('cache_portada', ['duration' => 0])) {?>
<div class="inicio">
    <section class="linea-tiempo">	
        <div class="scroll">
            <?php if ($notas_f): ?>
                <?php foreach ($notas_f as $key => $value): ?>
                    <article id="anio-<?= $key; ?>">
                        <h2 class="anio"><?= $key; ?></h2>
                        <ul class="portadas">
                            <?php foreach ($value as $n1910): ?>
                            <li class="tema_af" id="<?= $n1910['IdTema']?>">
                                <div class="fecha"> <?= Fechas::formato($n1910['Fecha'])?></div>
                                <div class="tema">
                                    <div class="imagen"><a href=''><img src="<?= Yii::$app->homeUrl."images/temas/".$n1910['Imagen']?>" alt="<?= $n1910['Titulo']?>"></a></div>
                                    <p class="titulo"><a href=''><?= $n1910['Titulo']?></a></p>
                                </div>
                                <div class="seccion seccionbg <?= String::removerAcentos($n1910['Seccion'], 1); ?>"><?= $n1910['Seccion']; ?></div>
                                <div class="tema-anio"><?= date('Y',strtotime($n1910['Fecha'])); ?></div>
                            </li>
                            <?php endforeach; ?>
                        </ul>	
                    </article>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </section>
</div>
<?php $this->endCache(); } ?>