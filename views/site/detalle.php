<?php 
    use app\helpers\String;
    use app\helpers\Fechas;?>  
<?php 
$this->registerJs("$(document.body).addClass('single ".String::removerAcentos($nota->Seccion,1)."');", \yii\web\View::POS_READY, 'my-options');
?>
<?php if ($this->beginCache('cache_detalle_'.$nota->idTema, ['duration' => 0])) {?>
<div class="interior detalle">
    <section class="seccion seccionbg <?= String::removerAcentos($nota->Seccion,1);?>">	
        <div class="info">
            <span class="detalle-seccion"><?= $nota->Seccion;?></span>
            <i class="fa fa-circle" aria-hidden="true"></i>
            <span class="fecha"> <?= Fechas::formato($nota->Fecha);?></span>
        </div>
        <h1 class="titulo"><?= $nota->Titulo;?></h1>
        <div class="imagen-detalle">
            <img src="<?= Yii::$app->homeUrl."images/temas/".$nota->ImagenDetalle;?>" alt="<?= $nota->Titulo;?>">
        </div>
        <div class="resumen"><?= $nota->Sumario;?></div>
    </section>
    <section class="grafica">
        <p class="intro-fijo">Se encontraron <strong> <?= $total?> notas</strong> a lo largo  de <strong>100 años</strong> relacionadas con <strong><?= $nota->Clave;?></strong></p>
        <div class="grafico">
            <div id="chart_div"></div>
        </div>
    </section>
    <section class="estadisticas">
        <p class="intro-fijo">Explora estadísticas sobre la  cobertura  de este tema <strong>durante  100 años</strong> y descubre un lado  de la historia  que no conocías:</p>
        <ul>
            <li class="fechas tfechas" data-id="<?= $nota->idTema;?>"><a href="">Cobertura</a></li>
            <li class="personajes tpersonajes" data-id="<?= $nota->idTema;?>"><a href="">Personajes</a></li>
            <li class="lugares tlugares" data-id="<?= $nota->idTema;?>"><a href="">Lugares</a></li>
            <li class="hechos thechos" data-id="<?= $nota->idTema;?>"><a href="">Hemeroteca</a></li>
        </ul>
    </section>
        
</div>
<?php $this->endCache(); } ?>
<?php $mayor = max($cuenta)+100;?>
<script>
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn('string', 'Año');
        dataTable.addColumn('number', 'Articulos');
        dataTable.addColumn({type: 'string', role: 'tooltip'});
        dataTable.addRows([
          <?php foreach($cuenta as $c => $v):?>
              ['<?= $c;?>', <?= $v;?>,'<?= $v;?> artículos encontrados en <?= $c;?>'],
          <?php endforeach;?>
        ]);
        
        var options = {
          title: '',
          hAxis: {title: '', ticks: [1916,2016],format:''},
          vAxis: {title: '', ticks: [0,<?= $mayor;?>],format:'', minValue: 0, maxValue: 200},
          legend: 'none',
          backgroundColor: { fill:'transparent' }
        };

        var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

        chart.draw(dataTable, options);
      }
</script>