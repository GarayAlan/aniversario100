<?php if ($this->beginCache('cache_footer1', ['duration' => 0])) {?>
    <footer class="center">
        <div class="anios">
            <ul class="nav">
                <li>Saltar a:</li>
                <li><a href="#anio-1910" class="active">1910</a></li>
                <li><a href="#anio-1920">1920</a></li>
                <li><a href="#anio-1930">1930</a></li>
                <li><a href="#anio-1940">1940</a></li>
                <li><a href="#anio-1950">1950</a></li>
                <li><a href="#anio-1960">1960</a></li>
                <li><a href="#anio-1970">1970</a></li>
                <li><a href="#anio-1980">1980</a></li>
                <li><a href="#anio-1990">1990</a></li>
                <li><a href="#anio-2000">2000</a></li>
                <li><a href="#anio-2010">2010</a></li>
            </ul>
        </div>
    </footer>
<?php $this->endCache(); } ?>