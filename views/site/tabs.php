<?php
use app\helpers\String;
use app\helpers\Fechas;
$arraycs['el-universal'] = array(
    '0' => '#2d6a8d',
    '1' => '#477d9b',
    '2' => '#7ca2b8',
    '3' => '#96b4c6',
    '4' => '#b0c7d4',
    '5' => '#e5ecf1'
);
$arraycs['ciudad'] = array(
    '0' => '#5ac3bd',
    '1' => '#6fcbc5',
    '2' => '#83d2ce',
    '3' => '#98dad6',
    '4' => '#ace1de',
    '5' => '#c1e9e6'
);
$arraycs['nacional'] = array(
    '0' => '#32b56a',
    '1' => '#4cbe7d',
    '2' => '#65c88f',
    '3' => '#7fd1a2',
    '4' => '#99dab4',
    '5' => '#b2e3c7'
);
$arraycs['cultura'] = array(
    '0' => '#f4731c',
    '1' => '#f58538',
    '2' => '#f79655',
    '3' => '#f8a871',
    '4' => '#fab98e',
    '5' => '#fbcbaa'
);
$arraycs['politica'] = array(
    '0' => '#ed251b',
    '1' => '#ef4038',
    '2' => '#f25c54',
    '3' => '#f47771',
    '4' => '#f6928d',
    '5' => '#f8adaa'
);
$arraycs['mundo'] = array(
    '0' => '#2d92c4',
    '1' => '#47a0cb',
    '2' => '#62add3',
    '3' => '#7cbbda',
    '4' => '#96c9e2',
    '5' => '#b0d6e9'
);
$arraycs['espectaculos'] = array(
    '0' => '#eec833',
    '1' => '#f0cf4d',
    '2' => '#f2d666',
    '3' => '#f4dd80',
    '4' => '#f6e499',
    '5' => '#f9eab3'
);
$arraycs['deportes'] = array(
    '0' => '#901fe8',
    '1' => '#9e3beb',
    '2' => '#ac57ee',
    '3' => '#ba73f1',
    '4' => '#c88ff3',
    '5' => '#d5abf6'
);
$seccionc = String::removerAcentos($nota->Seccion, 1);?>
<?php
$this->registerJs("$(document.body).addClass('single " . String::removerAcentos($nota->Seccion, 1) . "');", \yii\web\View::POS_READY, 'my-options');
?>
<div class="interior estadisticas">
    <section class="breadcrumbs tema fechas tema_af seccionbg" id="<?= $id; ?>">
        <h1>Estas explorando <strong><?= $nota->Titulo; ?></strong> por:</h1>
    </section>
    <section>
        <div id="tabs" class="tabs no-js">
            <div class="tabs-nav">
                <a href="#" class="fechas <?= ($active == 'fechas') ? 'is-active' : ''; ?> tabs-nav-link">
                    <span>Cobertura</span>
                </a>
                <a href="#" class="personajes tabs-nav-link">
                    <span>Personajes</span>
                </a>
                <a href="#" class="lugares tabs-nav-link">
                    <span>Lugares</span>
                </a>
                <a href="#" class="hechos tabs-nav-link">
                    <span>Hemeroteca</span>
                </a>
            </div>
            <div class="tab-fechas c-tab <?= ($active == 'fechas') ? 'is-active' : ''; ?>">
                <div class="c-tab-content">
                    <div class="intro-fijo">
                        <p>El Universal cubrió el tema <strong><?= $nota->Titulo; ?></strong> principalmente en la sección <strong><?= (strtoupper(($coberturamayor)));?></strong></p>
                    </div>
                    <div id="piechart" style="width: 50%; height: 400px;"></div>
                    <table>
                        <thead>
                            <tr>
                                <th>Sección</th>
                                <th>Menciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0;?>
<?php foreach ($cobertura as $c => $v): ?>
                                <tr>
                                    <td><span style="background-color:<?= $arraycs[$seccionc][$i]; ?>"></span><?= utf8_decode(strtoupper($c)); ?></td>
                                    <td><?= $v; ?></td>
                                </tr>
<?php $i++; endforeach; ?>
                        </tbody>
                    </table>    
                </div>
            </div>
            <div class="tab-personajes c-tab">
            <div class="c-tab-content">
                <?php if($personajes):?>
                    <div class="intro-fijo">
                        <p>Watson encontró <strong><?= count($personajeswt); ?> personajes</strong> relacionados al tema <strong><?= $nota->Titulo; ?></strong></p>
                    </div>
                    <div class="lista-personajes">
                        <ul>
                            <?php foreach($personajes as $persona):?>
                                <li>
                                <div class="posicion"></div>
                                <div class="imagen">
                                    <img alt="" src="<?= Yii::$app->homeUrl."images/personas/".$persona['imagen'];?>">
                                </div>
                                <div class="info">
                                    <div class="nombre giorgioBold"><?= $persona['nombre'];?></div>
                                    <div class="menciones"><strong><?= $persona['resultados'];?></strong> menciones</div>
                                    <div>
                                        <div id="chart_div_<?= $persona['nombre'];?>" style="width: 100%; height: 130px;"></div>
                                        <script>
                                            var data = google.visualization.arrayToDataTable([
                                                ['Año', 'Menciones'],
                                                <?php foreach($persona['anios'] as $a => $v):?>
                                                    ['<?= utf8_decode(strtoupper($a)); ?>', <?= $v; ?>],
                <?php endforeach;?>
          
          
        ]);

        var options = {
          vAxis: {minValue: 0, textPosition: 'none' },
          hAxis: { textPosition: 'none' },
          title: '',
          legend: 'none',
          backgroundColor: {fill: 'transparent'},
          colors:['<?= $arraycs[$seccionc][0]; ?>']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div_<?= $persona['nombre'];?>'));
        chart.draw(data, options);
                                        </script>
                                    </div>
                                </div>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                <?php endif;?>
                <?php if($personajeswt):?>
                    <table>
                        <thead>
                            <tr>
                                <th>Persona</th>
                                <th>Menciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            
<?php foreach ($personajeswt as $c => $v): ?>
                                <tr>
                                    <td><?= utf8_decode(strtoupper($c)); ?></td>
                                    <td><?= $v; ?></td>
                                </tr>
<?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif;?>
                
            </div>
        </div>
            <div class="tab-lugares c-tab">
            <div class="c-tab-content">
                <div class="intro-fijo">
                    <p>Watson encontró <strong><?= count($lugares); ?> lugares</strong> relacionados al tema <strong><?= $nota->Titulo; ?></strong></p>
                </div>
                <div id="regions_div2" style="width: 900px; height: 80%;"></div>
                <div id="regions_div" style="width: 900px; height: 80%;"></div>
                
            </div>
        </div>
            <div class="tab-hechos c-tab">
            <div class="c-tab-content">
                <div class="intro-fijo">
                    <p>Watson encontró <strong><?= count($hemeroteca); ?> notas</strong> relacionados al tema <strong><?= $nota->Titulo; ?></strong></p>
                </div>
                <ul class="hemeroteca">
                    <?php foreach ($hemeroteca as $n1910): ?>
                    <li class="recorte" data-id="<?= $n1910->idRecorte;?>">
                        <div class="tema">
                            <div class="imagen"><a href=''><img src="<?= Yii::$app->homeUrl."images/recortes/".$n1910->imagen?>" alt="<?= $n1910->nombre?>"></a></div>
                            <p class="titulo"><a href=''><?= $n1910->nombre?></a></p>
                            <div class="fecha"> <?= Fechas::formato($n1910->fecha)?></div>
                        </div>
                        <div class="seccion seccionbg <?= String::removerAcentos($n1910->seccion, 1); ?>"><?= $n1910->seccion; ?></div>
                    </li>
                    <?php endforeach; ?>
                </ul>
                <div>
                    <?php foreach ($hemeroteca as $sec):?>
                        <div class="detalle-hemeroteca" style="display:none" id="hm_<?= $sec->idRecorte;?>">
                            <div class="hem-main <?= String::removerAcentos($sec->seccion, 1); ?>" >
                            <div class="hem-head seccionbg">
                                <div class="hem-info">
                                    <a href="" class="back">
                                        <div>Hemeroteca</div>
                                    </a>
                                    <h1><?= $sec->nombre?></h1>
                                    <div class="info">
                                        <span class="detalle-seccion"><?= $sec->seccion; ?></span>
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                        <span class="fecha"> <?= Fechas::formato($sec->fecha)?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="hem-img">
                                <img src="<?= Yii::$app->homeUrl."images/recortes/".$sec->imagen?>" alt="" width="100%">
                            </div>
                            <div class="hem-content">
                                <?= nl2br($sec->text);?>
                            </div>
                        </div>
                        <div class="hem-otras">
                            <p class="intro-fijo">Noticias del tema <strong> <?= $nota->Titulo; ?></strong></p>
                            <ul class="hemeroteca otras">
                                <?php foreach ($hemeroteca as $r2): ?>
                                <li class="<?= String::removerAcentos($r2->seccion, 1);?> recorte" data-id="<?= $r2->idRecorte?>">
                                    <div class="tema otras">
                                        <p class="titulo"><a href=''><?= $r2->nombre?></a></p>
                                        <div class="info">
                                            <span class="detalle-seccion seccionc"><?= $r2->seccion?></span>
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                            <span class="fecha"> <?= Fechas::formato($r2->fecha)?></span>
                                        </div>
                                    </div>
                                    
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    
                </div>
            </div>
        </div>
    </div>
</div>
</section>

</div>
<script>
    var myTabs = tabs({
        el: '#tabs',
        tabNavigationLinks: '.tabs-nav-link',
        tabContentContainers: '.c-tab'
    });

    myTabs.init();
    myTabs.goToTab(<?= $n_tab; ?>);

    google.charts.setOnLoadCallback(drawChart);
    google.charts.setOnLoadCallback(drawRegionsMap);
    google.charts.setOnLoadCallback(drawRegionsMap2);
    
    function drawRegionsMap2() {

        var data = google.visualization.arrayToDataTable([
            ['Ciudad', 'Menciones'],
          <?php foreach ($lugares as $c => $v): ?>
                ['<?= utf8_decode(strtoupper($c)); ?>', <?= $v; ?>],
          <?php endforeach; ?>
        ]);

        var options = {
            title: '',
            legend: 'none',
            backgroundColor: {fill: 'transparent'},
            region: 'MX',
            resolution: 'provinces',
            colorAxis: {colors: ['<?= $arraycs[$seccionc][5]; ?>', '<?= $arraycs[$seccionc][0]; ?>'] }
            
        };

        var chart = new google.visualization.GeoChart(document.getElementById('regions_div2'));

        chart.draw(data, options);
      }

      function drawRegionsMap() {

        var data = google.visualization.arrayToDataTable([
          ['País', 'Menciones'],
          <?php foreach ($lugares as $c => $v): ?>
                ['<?= utf8_decode(strtoupper($c)); ?>', <?= $v; ?>],
          <?php endforeach; ?>
        ]);

        var options = {
            title: '',
            legend: 'none',
            backgroundColor: {fill: 'transparent'},
            colorAxis: {colors: ['<?= $arraycs[$seccionc][5]; ?>', '<?= $arraycs[$seccionc][0]; ?>'] }
            
        };

        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        chart.draw(data, options);
      }
    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Seccion', 'Menciones'],
<?php foreach ($cobertura as $c => $v): ?>
                ['<?= utf8_decode(strtoupper($c)); ?>', <?= $v; ?>],
<?php endforeach; ?>

        ]);

        var options = {
            title: '',
            legend: 'none',
            backgroundColor: {fill: 'transparent'},
            slices: {
                0: {color: '<?= $arraycs[$seccionc][0]; ?>'},
                1: {color: '<?= $arraycs[$seccionc][1]; ?>'},
                2: {color: '<?= $arraycs[$seccionc][2]; ?>'},
                3: {color: '<?= $arraycs[$seccionc][3]; ?>'},
                4: {color: '<?= $arraycs[$seccionc][4]; ?>'},
                5: {color: '<?= $arraycs[$seccionc][5]; ?>'}
            },
            tooltip: {trigger: 'none'},
            is3D: true,
            pieSliceText: 'none'
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }
</script>
