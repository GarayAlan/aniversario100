<?php
use app\helpers\String;
use app\components\HeaderWidget;?>    
<?php echo HeaderWidget::widget(['nota'=>$nota[0]]);?>
<div class="estadisticas">
    <section class="tema fechas">
		<h1>Estas explorando <a href=""><strong>Inauguración del Metro en DF</strong></a> por:</h1>
		
	</section>
	<section>
		<div id="tabs" class="tabs no-js">
        <div class="tabs-nav">
          <a href="#" class="fechas tabs-nav-link is-active">
            <span>Fechas</span>
          </a>
          <a href="#" class="tematica tabs-tematica tabs-nav-link">
            <span>Temática</span>
          </a>
          <a href="#" class="sentimientos tabs-nav-link">
            <span>Sentimientos</span>
          </a>
          <a href="#" class="lugares tabs-nav-link">
            <span>Lugares</span>
          </a>
          <a href="#" class="hechos tabs-nav-link">
            <span>Hechos</span>
          </a>
          <a href="#" class="personajes tabs-nav-link">
            <span>Personajes</span>
          </a>
        </div>
        <div class="tab-fechas c-tab is-active">
          <div class="c-tab-content">
            <div class="intro">
            	<p class="intro-fijo">Se obtuvieron <strong>36 fechas clave</strong> después de analizar <strong>88 notas</strong> a lo largo de <strong>100 años</strong> relacionadas con <strong>Inauguración del metro en D. F.</strong></p>
        	</div>
            <div class="content">
            	<div class="tabla">
                    <div class="tabla-head">
                		<div class="tabla-fila">
                			<div class="tabla-cabeza">Fecha</div>
                			<div class="tabla-cabeza">Contexto</div>
                		</div>
                    </div>
                    <div class="tabla-body">
                		<div class="tabla-fila">
                			<div class="tabla-celda"><strong>de 1977 a 1982</strong></div>
                			<div class="tabla-celda">La segunda fase de construcción fue de</div>
                		</div>
                		<div class="tabla-fila">
                			<div class="tabla-celda"><strong>de 1983 a 1985</strong></div>
                			<div class="tabla-celda">La tercera etapa corresponde</div>
                		</div>
                		<div class="tabla-fila">
                			<div class="tabla-celda"><strong>de 1985 a 1987</strong></div>
                			<div class="tabla-celda">La cuarta fase</div>
                		</div>
                		<div class="tabla-fila">
                			<div class="tabla-celda"><strong>de 1988 a 1994</strong></div>
                			<div class="tabla-celda">En la quinta etapa</div>
                		</div>
                		<div class="tabla-fila">
                			<div class="tabla-celda"><strong>de 1994 a 2000</strong></div>
                			<div class="tabla-celda">En la sexta</div>
                		</div>
                        <div class="tabla-fila">
                            <div class="tabla-celda"><strong>de 1994 a 2000</strong></div>
                            <div class="tabla-celda">En la sexta</div>
                        </div>
                        <div class="tabla-fila">
                            <div class="tabla-celda"><strong>de 1994 a 2000</strong></div>
                            <div class="tabla-celda">En la sexta</div>
                        </div>
                        <div class="tabla-fila">
                            <div class="tabla-celda"><strong>de 1994 a 2000</strong></div>
                            <div class="tabla-celda">En la sexta</div>
                        </div>
                        <div class="tabla-fila">
                            <div class="tabla-celda"><strong>de 1994 a 2000</strong></div>
                            <div class="tabla-celda">En la sexta</div>
                        </div>
                        <div class="tabla-fila">
                            <div class="tabla-celda"><strong>de 1994 a 2000</strong></div>
                            <div class="tabla-celda">En la sexta</div>
                        </div>
                    </div>
            	</div>
            </div>
          </div>
        </div>
        <div class="tab-tematica c-tab">
          <div class="c-tab-content">
            <div class="intro">
            	<p class="intro-fijo">A lo largo de <strong>100 años,</strong> se han escrito <strong>88 notas</strong> relacionas  con <strong>Inauguración del metro en D. F,</strong> con las siguientes temáticas principales:</p>
        	</div>
            <div class="content">
            </div>
          </div>
        </div>
        <div class="tab-sentimientos c-tab">
          <div class="c-tab-content">
            <div class="intro">
            	<p class="intro-fijo">Analizando los matices con que se habló de <strong>Inauguración del metro en D. F.</strong> en <strong>88 notas</strong> a lo largo de <strong>100 años,</strong> estos son los sentimientos expresados sobre este tema:</p>
        	</div>
            <div class="content">
                <div class="detalle-scroll">
            	   <img src="../../images/sentimientosGrafica.jpg" alt="">
               </div>
            </div>
          </div>
        </div>
        <div class="tab-lugares c-tab">
          <div class="c-tab-content">
            <div class="intro">
            	<p class="intro-fijo">En <strong>100 años</strong> de notas, dentro de las <strong>88 notas</strong> encontradas que se  relacionan con <strong>Inauguración del metro en D. F.</strong> estos son los lugares  con mayor número de menciones:</p>
        	</div>
            <div class="content">
            </div>
          </div>
        </div>
        <div class="tab-hechos c-tab">
          <div class="c-tab-content">
            <div class="intro">
            	<p class="intro-fijo">En <strong>100 años</strong> de notas, dentro de las <strong>88 notas</strong> encontradas que se  relacionan con <strong>Inauguración del metro en D. F.</strong> estos son los hechos  con mayor número de menciones:</p>
        	</div>
            <div class="content">
            </div>
          </div>
        </div>
        <div class="tab-personajes c-tab" id="personajes-content">
          <div class="c-tab-content">
            <div class="intro">
                <p class="intro-fijo">Los <strong>9 personajes</strong> más mencionados en las <strong>88 notas</strong> analizadas a lo largo de  <strong>100 años</strong>  relacionadas con <strong>Inauguración del metro en D. F.</strong></p>
                 <div class="content">
                    <div class="detalle-scroll lista-personajes">
                        <ul>
                            <li>
                                <div class="lista-datos">
                                    <div class="posicion"></div>
                                    <div class="imagen">
                                        <img src="../../images/marcelo.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="nombre">Marcelo Ebrard</div>
                                        <div class="menciones"><strong>16</strong> menciones en</div>
                                        <div class="relacionados"><strong>33</strong> artículos relacionados</div>
                                        <div class="opinion-grafica">
                                            <img src="../../images/opinionGrafica.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="lista-link">
                                    <a href="">Más menciones en <strong>2012</strong></a>
                                </div>
                            </li>
                            <li>
                                <div class="lista-datos">
                                    <div class="posicion"></div>
                                    
                                    <div class="info">
                                        <div class="nombre">Francisco Bojórquez</div>
                                        <div class="menciones"><strong>12</strong> menciones en</div>
                                        <div class="relacionados"><strong>24</strong> artículos relacionados</div>
                                        <div class="opinion-grafica">
                                            <img src="../../images/opinionGrafica.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="lista-link">
                                    <a href="">Más menciones en <strong>2012</strong></a>
                                </div>
                            </li>
                            <li>
                                <div class="lista-datos">
                                    <div class="posicion"></div>
                                    <div class="imagen">
                                        <img src="../../images/lance.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="nombre">Gustavo Díaz Ordaz</div>
                                        <div class="menciones"><strong>8</strong> menciones en</div>
                                        <div class="relacionados"><strong>14</strong> artículos relacionados</div>
                                        <div class="opinion-grafica">
                                            <img src="../../images/opinionGrafica.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="lista-link">
                                    <a href="">Más menciones en <strong>2012</strong></a>
                                </div>                            
                            </li>
                            <li>
                                <div class="lista-datos">
                                    <div class="posicion"></div>
                                    <div class="imagen">
                                        <img src="../../images/lance.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="nombre">Gustavo Díaz Ordaz</div>
                                        <div class="menciones"><strong>8</strong> menciones en</div>
                                        <div class="relacionados"><strong>14</strong> artículos relacionados</div>
                                        <div class="opinion-grafica">
                                            <img src="../../images/opinionGrafica.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="lista-link">
                                    <a href="">Más menciones en <strong>2012</strong></a>
                                </div>
                            </li>
                            <li>
                                <div class="lista-datos">
                                    <div class="posicion"></div>
                                    <div class="imagen">
                                        <img src="../../images/lance.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="nombre">Gustavo Díaz Ordaz</div>
                                        <div class="menciones"><strong>8</strong> menciones en</div>
                                        <div class="relacionados"><strong>14</strong> artículos relacionados</div>
                                        <div class="opinion-grafica">
                                            <img src="img/opinionGrafica.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="lista-link">
                                    <a href="">Más menciones en <strong>2012</strong></a>
                                </div>
                            </li>
                            <li>
                                <div class="lista-datos">
                                    <div class="posicion"></div>
                                    <div class="imagen">
                                        <img src="../../images/lance.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="nombre">Gustavo Díaz Ordaz</div>
                                        <div class="menciones"><strong>8</strong> menciones en</div>
                                        <div class="relacionados"><strong>14</strong> artículos relacionados</div>
                                        <div class="opinion-grafica">
                                            <img src="../../images/opinionGrafica.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="lista-link">
                                    <a href="">Más menciones en <strong>2012</strong></a>
                                </div>
                            </li>
                            <li>
                                <div class="lista-datos">
                                    <div class="posicion"></div>
                                    <div class="imagen">
                                        <img src="../../images/lance.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="nombre">Gustavo Díaz Ordaz</div>
                                        <div class="menciones"><strong>8</strong> menciones en</div>
                                        <div class="relacionados"><strong>14</strong> artículos relacionados</div>
                                        <div class="opinion-grafica">
                                            <img src="../../images/opinionGrafica.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="lista-link">
                                    <a href="">Más menciones en <strong>2012</strong></a>
                                </div>
                            </li>
                            <li>
                                <div class="lista-datos">
                                    <div class="posicion"></div>
                                    <div class="imagen">
                                        <img src="../../images/lance.jpg" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="nombre">Gustavo Díaz Ordaz</div>
                                        <div class="menciones"><strong>8</strong> menciones en</div>
                                        <div class="relacionados"><strong>14</strong> artículos relacionados</div>
                                        <div class="opinion-grafica">
                                            <img src="../../images/opinionGrafica.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="lista-link">
                                    <a href="">Más menciones en <strong>2012</strong></a>
                                </div>                            
                            </li>
                        </ul>
                    </div>
                    <div class="grafico">
                        <img src="../../images/opinionTemaGrafica.jpg" alt="">
                    </div>
                </div>
            </div>
          </div>
        </div>

      </div>
	</section>
</div>
	<footer>
		<div class="inicio">
			<a href="<?= Yii::$app->homeUrl;?>"> <i class="fa fa-arrow-circle-o-left"></i>Volver a la  <strong>línea del tiempo principal</strong></a>
		</div>
	</footer>
<script>
  var myTabs = tabs({
    el: '#tabs',
    tabNavigationLinks: '.tabs-nav-link',
    tabContentContainers: '.c-tab'
  });

  myTabs.init();
</script>