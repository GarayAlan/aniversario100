<?php
use app\helpers\String; 
use app\components\HeaderWidget;
?>    
<?php echo HeaderWidget::widget();?>
<?php if ($this->beginCache('cache_portada', ['duration' => 0])) {?>
<section class="linea-tiempo">	
    <div class="scroll">
        <?php if ($notas_f): ?>
            <?php foreach ($notas_f as $key => $value): ?>
                <article id="anio-<?= $key; ?>">
                    <h2 class="anio"><?= $key; ?></h2>
                    <ul class="portadas">
                        <?php foreach ($value as $n1910): ?>
                        <li>
                            <div class="fecha"> <?= \Yii::t('app', '{0,date,long}', $n1910->timestamp); ?></div>
                            <div class="tema">
                                <div class="imagen"><a href='<?php echo Yii::$app->urlManager->createUrl(['detalle/'.$n1910->id]);?>'><img src="<?= ($n1910->image_small_size != '') ? $n1910->image_small_size : 'images/aguila.png'; ?>" alt="<?= $n1910->title; ?>"></a></div>
                                <p class="titulo"><a href='<?php echo Yii::$app->urlManager->createUrl(['detalle/'.$n1910->id]);?>'><?= $n1910->title; ?></a></p>
                            </div>
                            <div class="seccion seccionbg <?= String::removerAcentos($n1910->main_section, 1); ?>"><?= $n1910->main_section; ?></div>
                            <div class="tema-anio"><?= date('Y',$n1910->timestamp); ?></div>
                        </li>
                        <?php endforeach; ?>
                    </ul>	
                </article>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</section>
<footer class="center">
    <div class="anios">
        <ul class="nav">
            <li><a href="#">Saltar a:</a></li>
            <li><a href="#anio-1910" class="active">1910</a></li>
            <li><a href="#anio-1920">1920</a></li>
            <li><a href="#anio-1930">1930</a></li>
            <li><a href="#anio-1940">1940</a></li>
            <li><a href="#anio-1950">1950</a></li>
            <li><a href="#">1960</a></li>
            <li><a href="#">1970</a></li>
            <li><a href="#">1980</a></li>
            <li><a href="#">1990</a></li>
            <li><a href="#">2000</a></li>
            <li><a href="#">2010</a></li>
        </ul>
    </div>
</footer>
<?php $this->endCache(); } ?>