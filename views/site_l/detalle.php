<?php 
    use app\helpers\String;
    use app\helpers\Fechas;?>  
<?php if ($this->beginCache('cache_detalle_'.$nota->idTema, ['duration' => 0])) {?>
<div class="interior detalle">
    <section class="seccion seccionbg <?= String::removerAcentos($nota->Seccion,1);?>">	
        <div class="info">
            <span class="detalle-seccion"><?= $nota->Seccion;?></span>
            <i class="fa fa-circle" aria-hidden="true"></i>
            <span class="fecha"> <?= Fechas::formato($nota->Fecha);?></span>
        </div>
        <h1 class="titulo"><?= $nota->Titulo;?></h1>
        <div class="imagen-detalle">
            <img src="<?= Yii::$app->homeUrl."images/temas/".$nota->ImagenDetalle;?>" alt="<?= $nota->Titulo;?>">
        </div>
        <div class="resumen"><?= $nota->Sumario;?></div>
    </section>
    <section class="grafica">
        <p class="intro-fijo">Se encontraron <strong> <?= $global->es_apiResponse->es_totalResults?> notas</strong> a lo largo  de <strong>100 años</strong> relacionadas con <strong><?= $nota->Clave;?></strong></p>
    </section>
    <section class="estadisticas">
        <p class="intro-fijo">Explora estadísticas sobre la  cobertura  de este tema <strong>durante  100 años</strong> y descubre un lado  de la historia  que no conocías:</p>
        <ul>
            <li class="fechas tfechas" data-id="<?= $nota->idTema;?>"><a href="">Fechas</a></li>
            <li class="tematica ttematica" data-id="<?= $nota->idTema;?>"><a href="">Temática</a></li>
            <li class="sentimientos tsentimientos" data-id="<?= $nota->idTema;?>"><a href="">Sentimientos</a></li>
            <li class="lugares tlugares" data-id="<?= $nota->idTema;?>"><a href="">Lugares</a></li>
            <li class="hechos thechos" data-id="<?= $nota->idTema;?>"><a href="">Hechos</a></li>
            <li class="personajes tpersonajes" data-id="<?= $nota->idTema;?>"><a href="">Personajes</a></li>
        </ul>
    </section>
        
</div>
<?php $this->endCache(); } ?>