<div class="interior estadisticas">
    <section class="tema fechas deportes tema_af" id="<?= $id;?>">
        <h1>Estas explorando <strong><?= $nota->Titulo;?></strong> por:</h1>
    </section>
    <section>
        <div id="tabs" class="tabs no-js">
            <div class="tabs-nav">
                <a href="#" class="fechas <?=($active == 'fechas') ? 'is-active':'';?> tabs-nav-link">
                    <span>Fechas</span>
                </a>
                <a href="#" class="tematica tabs-nav-link">
                    <span>Temática</span>
                </a>
                <a href="#" class="sentimientos tabs-nav-link">
                    <span>Sentimientos</span>
                </a>
                <a href="#" class="lugares tabs-nav-link">
                    <span>Lugares</span>
                </a>
                <a href="#" class="hechos tabs-nav-link">
                    <span>Hechos</span>
                </a>
                <a href="#" class="personajes tabs-nav-link">
                    <span>Personajes</span>
                </a>
            </div>
            <div class="tab-fechas c-tab <?=($active == 'fechas') ? 'is-active':'';?>">
                <div class="c-tab-content">
                    <div class="intro-fijo">
                        <h1>Datos de Watson</h1>
                        <p>Se obtuvieron <strong><?= count(@$fechas->es_apiResponse->ibmsc_facet->ibmsc_facetValue);?> fechas clave</strong> </p>
                        <ul>
                            <?php if(isset($fechas->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                                <?php foreach(@$fechas->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $fechas_facet):?>
                                    <li>
                                        <h4><?= @utf8_decode($fechas_facet->label);?> -> <small><?= @$fechas_facet->weight;?> coincidencias</small></h4>
                                    </li>
                                <?php endforeach;?>
                            <?php else:?>
                                    <li>No se encontraron Fechas</li>
                            <?php endif;?>
                        </ul>
                    </div>
                    <!--div class="intro-fijo">
                        <p>Se obtuvieron <strong>36 fechas clave</strong> después de analizar <strong>88 notas</strong> a lo largo de <strong>100 años</strong> relacionadas con <strong>Inauguración del metro en D. F.</strong></p>
                    </div>
                    
                    <div class="content">
                        <div class="tabla">
                            <div class="tabla-fila">
                                <div class="tabla-cabeza">Fecha</div>
                                <div class="tabla-cabeza">Contexto</div>
                            </div>
                            
                            <div class="tabla-fila">
                                <div class="tabla-celda"><strong>de 1977 a 1982</strong></div>
                                <div class="tabla-celda">La segunda fase de construcción fue de</div>
                            </div>
                            <div class="tabla-fila">
                                <div class="tabla-celda"><strong>de 1983 a 1985</strong></div>
                                <div class="tabla-celda">La tercera etapa corresponde</div>
                            </div>
                            <div class="tabla-fila">
                                <div class="tabla-celda"><strong>de 1985 a 1987</strong></div>
                                <div class="tabla-celda">La cuarta fase</div>
                            </div>
                            <div class="tabla-fila">
                                <div class="tabla-celda"><strong>de 1988 a 1994</strong></div>
                                <div class="tabla-celda">En la quinta etapa</div>
                            </div>
                            <div class="tabla-fila">
                                <div class="tabla-celda"><strong>de 1994 a 2000</strong></div>
                                <div class="tabla-celda">En la sexta</div>
                            </div>
                        </div>
                    </div-->
                </div>
            </div>
            <div class="tab-tematica c-tab">
                <div class="c-tab-content">
                    <!--div class="intro-fijo">
                        <p>A lo largo de <strong>100 años,</strong> se han escrito <strong>88 notas</strong> relacionas  con <strong>Inauguración del metro en D. F,</strong> con las siguientes temáticas principales:</p>
                    </div>
                    <div class="content">
                    </div-->
                    <h1>Datos de Watson</h1>
                        <p>Se obtuvieron <strong><?= count(@$tematica->es_apiResponse->ibmsc_facet->ibmsc_facetValue);?> tematicas</strong> </p>
                    <ul>
            <?php if(isset($tematica->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                <?php foreach(@$tematica->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $tematica_facet):?>
                    <li>
                        <h4><?= @utf8_decode($tematica_facet->label);?> -> <small><?= @$tematica_facet->weight;?> coincidencias</small></h4>
                        
                    </li>
                <?php endforeach;?>
            <?php else:?>
                    <li>No se encontraron Tematicas</li>
            <?php endif;?>
        </ul>
                </div>
            </div>
            <div class="tab-sentimientos c-tab">
                <div class="c-tab-content">
                    
                </div>
            </div>
            <div class="tab-lugares c-tab">
                <div class="c-tab-content">
                    <h1>Datos de Watson</h1>
                        <p>Se obtuvieron <strong><?= count(@$lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue);?> lugares</strong> </p>
                    <ul>
            <?php if(isset($lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                <?php foreach(@$lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $lugares_facet):?>
                    <li>
                        <h4><?= @utf8_decode($lugares_facet->label);?> -> <small><?= @$lugares_facet->weight;?> coincidencias</small></h4>
                        
                    </li>
                <?php endforeach;?>
            <?php else:?>
                    <li>No se encontraron Lugares</li>
            <?php endif;?>
        </ul>
                </div>
            </div>
            <div class="tab-hechos c-tab">
                <div class="c-tab-content">
                    <h1>Datos de Watson</h1>
                        <p>Se obtuvieron <strong><?= count(@$hechos->es_apiResponse->ibmsc_facet->ibmsc_facetValue);?> hechos</strong> </p>
                    <ul>
            <?php if(isset($hechos->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                <?php foreach(@$hechos->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $hechos_facet):?>
                    <li>
                        <h4><?= @utf8_decode($hechos_facet->label);?> -> <small><?= @$hechos_facet->weight;?> coincidencias</small></h4>
                    </li>
                <?php endforeach;?>
            <?php else:?>
                    <li>No se encontraron Hechos</li>
            <?php endif;?>
        </ul>
                </div>
            </div>
            <div class="tab-personajes c-tab">
                <div class="c-tab-content">
                   <h1>Datos de Watson</h1>
                        <p>Se obtuvieron <strong><?= count(@$personajes->es_apiResponse->ibmsc_facet->ibmsc_facetValue);?> personajes</strong> </p>
                    <ul>
            <?php if(isset($personajes->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                <?php foreach(@$personajes->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $personajes_facet):?>
                    <li>
                        <h4><?= @utf8_decode($personajes_facet->label);?> -> <small><?= @$personajes_facet->weight;?> coincidencias</small></h4>
                    </li>
                <?php endforeach;?>
            <?php else:?>
                    <li>No se encontraron Personajes</li>
            <?php endif;?>
        </ul>
                </div>
            </div>

        </div>
    </section>

</div>
<script>
    var myTabs = tabs({
        el: '#tabs',
        tabNavigationLinks: '.tabs-nav-link',
        tabContentContainers: '.c-tab'
    });

    myTabs.init();
    myTabs.goToTab(<?=$n_tab;?>);
</script>