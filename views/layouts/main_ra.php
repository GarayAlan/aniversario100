    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Aniversario 100</title>
            <link rel="stylesheet" href="./index_files/font-awesome.min.css">
            <link href="./index_files/css" rel="stylesheet" type="text/css">
            <script type="text/javascript" src="./index_files/jquery.min.js"></script>
            <script type="text/javascript" src="./index_files/scroll.js"></script>
            <script type="text/javascript" src="./index_files/functions.js"></script>
            <script type="text/javascript" src="./index_files/tabs.js"></script>
            <script type="text/javascript" src="./index_files/sparkline.js"></script>
            <script type="text/javascript" src="./index_files/loader.js"></script>
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
            <script type="text/javascript" src="https://www.google.com/jsapi"></script>
            <script>
                google.charts.load('current', {'packages':['corechart','geochart']});
            </script>
            
            <script src="./js/sparkline.js"></script>
            <link id="load-css-0" rel="stylesheet" type="text/css" href="./index_files/util.css">
            <link rel="stylesheet" href="./style/css/index.css">
            <link id="load-css-1" rel="stylesheet" type="text/css" href="./index_files/tooltip.css">
        </head>
        <body data-feedly-mini="yes">
            <?= $content ?>
            <script type="text/javascript" src="./index_files/jsapi_compiled_format_module.js"></script>
            <script type="text/javascript" src="./index_files/jsapi_compiled_default_module.js"></script>
            <script type="text/javascript" src="./index_files/jsapi_compiled_ui_module.js"></script>
            <script type="text/javascript" src="./index_files/jsapi_compiled_corechart_module.js"></script>
            <script src="./js/raidho.js"></script>
            
        </body>
    </html>