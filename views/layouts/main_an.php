<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Aniversario 100</title>
	<link rel="stylesheet" href="<?= yii\helpers\Url::base();?>/fonts/font-awesome/css/font-awesome.min.css">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script type="text/javascript" src="<?= yii\helpers\Url::base();?>/js/scroll.js"></script>
        <script type="text/javascript" src="<?= yii\helpers\Url::base();?>/js/functions.js"></script>
        <script type="text/javascript" src="<?= yii\helpers\Url::base();?>/js/tabs.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script>
            google.charts.load('current', {'packages':['corechart']});
        </script>
        <link rel="stylesheet" href="<?= yii\helpers\Url::base();?>/css/estilos.css">
    </head>
    <!-- <body class="<?php echo $this->params['bclass']; ?>"> -->
    <body>
        <?= $content ?>
        		
        
    </body>
</html>