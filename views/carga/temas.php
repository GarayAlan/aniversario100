    <?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    
    ?>
    <div class="video-create">
	<div class="page-header clearfix">	
    	<h1><?= Html::encode($this->title) ?></h1>
        </div>
        <?php 
        $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal'],
        ]) ?>
        <?= $form->field($model,'Fecha')?>
        <?= $form->field($model,'Titulo')?>
        <?= $form->field($model,'Imagen')?>
        <?= $form->field($model,'Sumario')?>
        <?= $form->field($model,'Seccion')?>
        <div class="form-group">
            <div class="col-lg-11">
                <?= Html::submitButton('Guardar Tema', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>