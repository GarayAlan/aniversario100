<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use common\models\Seccion;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\widgets\Datepicker;
use kartik\datecontrol\DateControl;
use kartik\checkbox\CheckboxX;

use yii\web\JsExpression;

$url = \yii\helpers\Url::to(['search']);
$urlCh = \yii\helpers\Url::to(['canales/search']);

/* @var $this yii\web\View */
/* @var $model videos\models\Video */
/* @var $form yii\widgets\ActiveForm */
?>
<?php

    $js = "
            if($('#video-programar').val() == 1){                                
                $('.field-video-fechaprogramadapublicacion').removeClass('hide');
            }else{                    
                $('.field-video-fechaprogramadapublicacion').addClass('hide');
            }

            $('#frmVideo').on('afterValidate',function(e){                                  
                if(!$('#frmVideo').find('.has-error').length) {
                     $.blockUI();
                }
            });

            /*$('#frmVideo').submit(function(e){                
                e.preventDefault();
                e.stopImmediatePropagation(); 
                if(!$('#frmVideo').find('.has-error').length) {
                    submitFormPost('#frmVideo');        
                }                
                return false;
            });*/ ";

    $js2 = "
            $('.field-video-fechaprogramadapublicacion').addClass('hide');
    
            $('#frmVideo').on('afterValidate',function(e){                                  
                if(!$('#frmVideo').find('.has-error').length) {
                     $.blockUI();
                }
            });

            $('#video-mp4').change(function() {                
                
                var vExt = $(this).val().split('.').pop().toLowerCase();

                if($(this).val() != '' && (vExt == 'mov' || vExt == 'mp4')){
                    $.blockUI();
                }else{
                    sc_y2_Notification('Mensaje:','El formato '+ vExt +' no es aceptado, seleccione un video con formato mov o mp4.','error',null);                    
                    $('#video-mp4').fileinput('clear');                    
                }
                
            });

            $('#video-thumbnail').change(function() {                
                
                var vExt = $(this).val().split('.').pop().toLowerCase();

                if($(this).val() != '' && (vExt == 'png' || vExt == 'jpg' || vExt == 'jpeg')){
                    $.blockUI();
                }else{
                    sc_y2_Notification('Mensaje:','El formato '+ vExt +' no es aceptado, seleccione una imagen con formato png o jpg.','error',null);                    
                    $('#video-thumbnail').fileinput('clear');                    
                }

            });

             ";

    if(!$model->isNewRecord)
        $this->registerJs($js);
    else
        $this->registerJs($js2);

?>

<div class="video-form" style="padding-bottom:50px;">
    <?php

        $form = ActiveForm::begin( [ 'type' => ActiveForm::TYPE_VERTICAL,
            'options' => ['id' => 'frmVideo', 'enctype' => 'multipart/form-data',] ] );        

        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [  
                [
                    'attributes' => [
                        //'IdExterno' => ['type' => Form::INPUT_TEXT,'options' => [ 'disabled' => 'disabled'], ],
                        'KalturaId' => ['type' => Form::INPUT_TEXT,'options' => [ 'disabled' => 'disabled'], ],
                    ],
                ],                    
                [
                    'attributes' => [
                        'Titulo' => [
                            'type' => Form::INPUT_TEXT,                            
                            'options' => [  
                                'maxlength' => true,
                            ],      
                        ],
                        'SeccionId' => [   
                            'type' => Form::INPUT_DROPDOWN_LIST,
                            'items' => ArrayHelper::map ( Seccion::find()->all(), 'SeccionId', 'Descripcion'),
                            'options' => [                                  
                                'prompt' => 'Seleccionar...',
                            ],      
                        ],
                    ]
                ], 
                [
                    'attributes' => [
                        'Sumario' => ['type' => Form::INPUT_TEXTAREA,],
                    ],
                ],     
                [
                    'attributes' =>[
                        'Mp4' => [
                                'name' => 'Video[Mp4]',
                                'type'=>Form::INPUT_WIDGET,
                                'widgetClass'=>'\kartik\widgets\FileInput',
                                'options' => [                        
                                    'options' => ['accept' => 'video/mp4, .mov','multiple'=>false,],
                                    'pluginOptions' => [
                                        'showRemove' => false,
                                        'showUpload' => false,
                                        'overwriteInitial'=> true,   
                                        'initialPreview'=> $model->getHtmlPreview(),
                                        'initialCaption'=> (!is_null($model->Mp4) ?  strrchr($model->Mp4,"/") : '') ,
                                    ],                                    
                                    'pluginEvents' => [                                            
                                            "fileloaded" => "function() { $.unblockUI(); }",                                                                                                                                    
                                    ],

                                ],

                        ], 
                        'Thumbnail' => [
                                'type'=>Form::INPUT_WIDGET,
                                'widgetClass'=>'\kartik\widgets\FileInput',
                                'options' => [                                     
                                    'options' => ['accept' => 'image/jpeg, image/jpg','multiple'=>false, /*((!$model->isNewRecord) ? 'disabled' : '') => ((!$model->isNewRecord) ? 'disabled' : ''),*/],
                                    'pluginOptions' => [
                                        'showRemove' => false,
                                        'showUpload' => false,
                                        'overwriteInitial'=> true,   
                                        'initialPreview'=> $model->getThumbnailPreview(),
                                        'initialCaption'=> (!is_null($model->Thumbnail) ?  strrchr($model->Thumbnail,"/") : '') ,
                                    ],
                                    'pluginEvents' => [                                            
                                            "fileloaded" => "function() { $.unblockUI(); }",                                                                                                                                    
                                    ],
                                ],

                        ],                         
                    ]
                ],
                [
                    'attributes' => [
                        'Tags' => ['type' => Form::INPUT_TEXTAREA,'options' => ['maxlength' => true,]],    
                    ],
                ], 
                [
                    'attributes' => [ 
                        'Publicidad' => [
                                'type'=>Form::INPUT_WIDGET,
                                'widgetClass'=>'kartik\checkbox\CheckboxX',
                                'label' => '',                                
                                'options' => [  
                                    'autoLabel' => true,
                                    'pluginOptions'=>['threeState'=>false],                                    
                                ],             
                        ],
                        'Mxm' => [
                                'type'=>Form::INPUT_WIDGET,
                                'widgetClass'=>'kartik\checkbox\CheckboxX',
                                'label' => '',                                
                                'options' => [  
                                    'autoLabel' => true,
                                    'pluginOptions'=>['threeState'=>false],                                    
                                ],             
                        ],     
                        'Agencia' => [
                                'type'=>Form::INPUT_WIDGET,
                                'widgetClass'=>'kartik\checkbox\CheckboxX',
                                'label' => '',                                
                                'options' => [  
                                    'autoLabel' => true,
                                    'pluginOptions'=>['threeState'=>false],                                    
                                ],             
                        ],     
                        'EnviarLimelight' => [
                                'type'=>Form::INPUT_WIDGET,
                                'widgetClass'=>'kartik\checkbox\CheckboxX',
                                'label' => '',                                
                                'options' => [  
                                    'readonly'=> !$model->isNewRecord,
                                    'autoLabel' => true,
                                    'pluginOptions'=>['threeState'=>false],                                    
                                ],             
                        ],     
                    ],
                ], 
                [
                    'attributes' => [
                        'Relacionados' => [
                                'type'=> Form::INPUT_WIDGET,                                             
                                'widgetClass'=>'\kartik\widgets\Select2',                                
                                'options' => [             
                                    'size' => Select2::MEDIUM,          
                                    'initValueText' => $model->RelacionadosTitulos,
                                    'options' => [
                                        'multiple'=> true,
                                        'placeholder' => 'Buscar video para relacionar ...'
                                    ],
                                    'pluginOptions' => [                                        
                                        'minimumInputLength' => 3,
                                        'multiple'=> true,
                                        'ajax' => [
                                            'url' => $url,
                                            'dataType' => 'json',
                                            'data' => new JsExpression('function(params) { return {q:params.term, id:'.(isset($model->VideoId) ? $model->VideoId : 0).'}; }')                                            
                                        ],
                                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                        'templateResult' => new JsExpression("function(video) { return video.text; }"),
                                        'templateSelection' => new JsExpression('function (video) { return video.text; }'),
                                    ],
                                ],
                        ],
                        'Canales' => [
                                'type'=> Form::INPUT_WIDGET,                                             
                                'widgetClass'=>'\kartik\widgets\Select2',                                
                                'options' => [             
                                    'size' => Select2::MEDIUM,          
                                    'initValueText' => $model->CanalesTitulos,
                                    'options' => [
                                        'multiple'=> true,
                                        'placeholder' => 'Buscar exclusiva para relacionar ...'
                                    ],
                                    'pluginOptions' => [                                        
                                        'minimumInputLength' => 3,
                                        'multiple'=> true,
                                        'ajax' => [
                                            'url' => $urlCh,
                                            'dataType' => 'json',
                                            'data' => new JsExpression('function(params) { return {q:params.term, id:'.(isset($model->VideoId) ? $model->VideoId : 0).'}; }')                                            
                                        ],
                                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                        'templateResult' => new JsExpression("function(video) { return video.text; }"),
                                        'templateSelection' => new JsExpression('function (video) { return video.text; }'),
                                    ],
                                ],
                        ],                      
                    ],
                ], 

                [
                    'attributes' => [  
                        'Programar' => [
                                'type'=>Form::INPUT_WIDGET,
                                'widgetClass'=>'kartik\checkbox\CheckboxX',
                                'label' => '',                                
                                'options' => [  
                                    'readonly'=> !$model->isNewRecord,
                                    'autoLabel' => true,
                                    'pluginOptions'=>['threeState'=>false],
                                    'pluginEvents'=>[
                                        "change"=>"function() {                                            
                                            if($('#video-programar').val() == 1){                                
                                                $('.field-video-fechaprogramadapublicacion').removeClass('hide');
                                            }else{                    
                                                $('.field-video-fechaprogramadapublicacion').addClass('hide');
                                            }

                                        }",
                                    ],
                                ],             
                        ],  
                    ],
                ],
                [
                    'attributes' => [                                                
                        'FechaProgramadaPublicacion' => [
                                'type'=>Form::INPUT_WIDGET,
                                'widgetClass'=> 'kartik\datecontrol\DateControl',
                                'options' => [
                                    'disabled'=> ($model->EstatusId == 3),
                                    'options' => ['class' => 'hide',],
                                    'displayFormat' => 'php:d-M-Y H:i:s',                                    
                                    'type'=>DateControl::FORMAT_DATETIME,
                                ],
                            ],            
                    ],
                ],

            ],
        ]);

    ?>
    <div class="errors" >
    </div>
    <div class="form-group">
        <br/><br/>        
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['id'=>'btnGuardaVideo','class' => ($model->isNewRecord ? 'btn btn-success' : 'btn btn-primary').' pull-right']) ?>
        <br/><br/>
    </div>
    <?php ActiveForm::end(); ?>
    <div style="height:100px"></div>
</div>
