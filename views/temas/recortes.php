<br><br><br>
<h1>Recortes</h1>
<hr>
<div class="row">
    <?php foreach ($recortes as $recorte): ?>
        <div class="col-xs-6 col-md-3">
            <a href="#" class="thumbnail">
                <img src="<?= Yii::$app->homeUrl . "images/recortes/" . $recorte->nombre; ?>" alt="...">
            </a>
            <br>
            <a href="" class="btn btn-danger">Quitar este recorte</a>
        </div>
    <?php endforeach; ?>
</div>