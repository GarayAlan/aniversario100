<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Temas */

$this->title = 'Actualizar Tema: ' . $model->idTema;
$this->params['breadcrumbs'][] = ['label' => 'Temas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idTema, 'url' => ['view', 'id' => $model->idTema]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="temas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
