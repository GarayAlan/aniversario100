<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TemasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Temas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="temas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Temas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idTema',
            'Fecha',
            'Imagen',
            'Titulo',
            'Sumario:ntext',
            'Seccion',
            'Clave',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{update}{delete}{watson}{recortes}',
                'buttons'=>[
                    'watson'=>function ($url, $model) {     
                                return Html::a('<span class="glyphicon glyphicon-align-justify"></span>', $url, [
                                        'title' => 'Consultar información del tema en Watson',
                                ]);                                
            
                    },
                            'recortes'=>function ($url, $model) {     
                                return Html::a('<span class="glyphicon glyphicon-camera"></span>', $url, [
                                        'title' => 'Recortes',
                                ]);                                
            
                              }

                ]],
        ],
    ]); ?>
</div>
