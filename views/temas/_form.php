<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;?>
<div class="temas-form">
    <?php $form = ActiveForm::begin(['enableClientValidation'=>false,'options' => ['enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'Fecha')->textInput(['maxlength' => 10])->hint('En formato YYYY-MM-DD'); ?>
    <?= $form->field($model, 'Imagen')->fileInput()->hint('Esta imagen debe tener un tamaño de 220px X 220px') ?>
    <?= $form->field($model, 'ImagenDetalle')->fileInput()->hint('Esta imagen debe tener un tamaño de 160px X 95px') ?>
    <?= $form->field($model, 'Titulo')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'Clave')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'Sumario')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'Seccion')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>