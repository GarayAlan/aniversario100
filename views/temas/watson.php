<br><br><br>
<h1>Buscando la frase <b><?= $clave;?></b> en Watson</h1>
<h3>Total de resultados = <?= $anio->es_apiResponse->es_totalResults?></h3>
<hr>
<div class="row">
    <div class="col-lg-4">
        <h2>Años</h2>
        <hr>
        <ul>
            <ul>
            <?php if(isset($anio->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                <?php if(is_array($anio->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                    <?php foreach($anio->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $anio_facet):?>
                        <li>
                            <h4><?= @utf8_decode($anio_facet->label);?></h4>
                            <h6> <?= $anio_facet->weight;?> coincidencias</h6>
                        </li>
                    <?php endforeach;?>
                <?php else:?>
                        <li>
                            <h4><?= @utf8_decode($anio->es_apiResponse->ibmsc_facet->ibmsc_facetValue->label);?></h4>
                            <h6> <?= $anio->es_apiResponse->ibmsc_facet->ibmsc_facetValue->weight;?> coincidencias</h6>
                        </li>
                <?php endif;?>
            
            
                
            <?php else:?>
                    <li>No se encontraron Fechas</li>
            <?php endif;?>
        </ul>
    </div>
    <div class="col-lg-4">
        <h2>Fechas</h2>
        <hr>
        <ul>
            <ul>
            <?php if(isset($fechas->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                <?php if(is_array($fechas->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                    <?php foreach($fechas->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $fechas_facet):?>
                        <li>
                            <h4><?= @utf8_decode($fechas_facet->label);?></h4>
                            <h6> <?= @$fechas_facet->weight;?> coincidencias</h6>
                        </li>
                    <?php endforeach;?>
                <?php else:?>
                        <li>
                            <h4><?= @utf8_decode($fechas->es_apiResponse->ibmsc_facet->ibmsc_facetValue->label);?></h4>
                            <h6> <?= $fechas->es_apiResponse->ibmsc_facet->ibmsc_facetValue->weight;?> coincidencias</h6>
                        </li>
                <?php endif;?>
            
            
                
            <?php else:?>
                    <li>No se encontraron Fechas</li>
            <?php endif;?>
        </ul>
    </div>
    <div class="col-lg-4">
        <h2>Secciones</h2>
        <hr>
        <ul>
            <?php if(isset($tematica->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                <?php if(is_array($tematica->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                    <?php foreach(@$tematica->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $tematica_facet):?>
                        <li>
                            <h4><?= @utf8_decode($tematica_facet->label);?></h4>
                            <h6> <?= @$tematica_facet->weight;?> coincidencias</h6>
                        </li>
                    <?php endforeach;?>
                <?php else:?>
                        <li>
                            <h4><?= @utf8_decode($tematica->es_apiResponse->ibmsc_facet->ibmsc_facetValue->label);?></h4>
                            <h6> <?= @$tematica->es_apiResponse->ibmsc_facet->ibmsc_facetValue->weight;?> coincidencias</h6>
                        </li>
                <?php endif;?>
            
            
                
            <?php else:?>
                    <li>No se encontraron Tematicas</li>
            <?php endif;?>
        </ul>
    </div>
    <div class="col-lg-4">
        <h2>Sentimientos</h2>
        <hr>
        <ul>
            <?php if(isset($sentimientos->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                <?php if(is_array($sentimientos->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                    <?php foreach($sentimientos->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $sentimientos_facet):?>
                        <li>
                            <h4><?= @utf8_decode($sentimientos_facet->label);?></h4>
                            <h6> <?= $sentimientos_facet->weight;?> coincidencias</h6>
                        </li>
                    <?php endforeach;?>
                <?php else:?>
                        <li>
                            <h4><?= @utf8_decode($sentimientos->es_apiResponse->ibmsc_facet->ibmsc_facetValue->label);?></h4>
                            <h6> <?= $sentimientos->es_apiResponse->ibmsc_facet->ibmsc_facetValue->weight;?> coincidencias</h6>
                        </li>
                <?php endif;?>
            <?php else:?>
                    <li>No se encontraron sentimientos</li>
            <?php endif;?>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <h2>Lugares</h2>
        <hr>
        <ul>
            <?php if(isset($lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                <?php if(is_array($lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                    <?php foreach($lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $lugares_facet):?>
                        <li>
                            <h4><?= @utf8_decode($lugares_facet->label);?></h4>
                            <h6> <?= $lugares_facet->weight;?> coincidencias</h6>
                        </li>
                    <?php endforeach;?>
                <?php else:?>
                        <li>
                            <h4><?= @utf8_decode($lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue->label);?></h4>
                            <h6> <?= $lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue->weight;?> coincidencias</h6>
                        </li>
                <?php endif;?>
            <?php else:?>
                    <li>No se encontraron lugares</li>
            <?php endif;?>
        </ul>
    </div>
    <div class="col-lg-4">
        <h2>Personajes</h2>
        <hr>
        <ul>
            <?php if(isset($personajes->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                <?php if(is_array($personajes->es_apiResponse->ibmsc_facet->ibmsc_facetValue)):?>
                    <?php foreach($personajes->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $personajes_facet):?>
                        <li>
                            <h4><?= @utf8_decode($personajes_facet->label);?></h4>
                            <h6> <?= $personajes_facet->weight;?> coincidencias</h6>
                        </li>
                    <?php endforeach;?>
                <?php else:?>
                        <li>
                            <h4><?= @utf8_decode($personajes->es_apiResponse->ibmsc_facet->ibmsc_facetValue->label);?></h4>
                            <h6> <?= $personajes->es_apiResponse->ibmsc_facet->ibmsc_facetValue->weight;?> coincidencias</h6>
                        </li>
                <?php endif;?>
            <?php else:?>
                    <li>No se encontraron personajes</li>
            <?php endif;?>
        </ul>
    </div>
</div>
<div class="row">
    <h1>Url de los servicios REST consultados <small>Para verificar la salida en bruto de Watson</small></h1>
    <hr>
    <h3>Consulta global <br><small><?= @$urlg;?></small></h3>
    <h3>Faceta Fechas <br><small><?= @$urlff;?></small></h3>
    <h3>Faceta Tematica <br><small><?= @$urlft;?></small></h3>
    <h3>Faceta Sentimientos <br><small><?= @$urlfs;?></small></h3>
    <h3>Faceta Lugares <br><small><?= @$urlfl;?></small></h3>
    <h3>Faceta Personajes <br><small><?= @$urlfp;?></small></h3>
    <h3>Faceta Años <br><small><?= @$urlan;?></small></h3>
</div>
<style>
    .my-table {
    table-layout: fixed;
    word-wrap: break-word;
}
</style>