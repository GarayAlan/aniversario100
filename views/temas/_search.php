<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TemasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="temas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idTema') ?>

    <?= $form->field($model, 'Fecha') ?>

    <?= $form->field($model, 'Imagen') ?>

    <?= $form->field($model, 'Titulo') ?>

    <?= $form->field($model, 'Sumario') ?>

    <?php // echo $form->field($model, 'Seccion') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
