<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personajes".
 *
 * @property integer $idPersona
 * @property integer $idTema
 * @property string $nombre
 * @property string $imagen
 */
class Personajes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'personajes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTema', 'nombre','imagen'], 'required'],
            [['imagen'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTema' => 'Id Tema',
            'nombre' => 'Nombre',
            'imagen' => 'Imagen'
        ];
    }
}
