<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recortes".
 *
 * @property integer $idRecorte
 * @property integer $idTema
 * @property string $nombre
 * @property string $imagen
 * @property string $seccion
 * @property string $fecha
 * @property string $text
 */
class Recortes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recortes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTema', 'nombre'], 'required'],
            [['nombre'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTema' => 'Id Tema',
            'nombre' => 'Imagen'
        ];
    }
}
