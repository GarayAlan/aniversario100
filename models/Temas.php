<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "temas".
 *
 * @property integer $idTema
 * @property string $Fecha
 * @property string $Imagen
 * @property string $ImagenDetalle
 * @property string $Titulo
 * @property string $Sumario
 * @property string $Seccion
 */
class Temas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'temas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Fecha', 'Titulo', 'Sumario', 'Seccion','Clave','Imagen','ImagenDetalle'], 'required'],
            [['Fecha'], 'safe'],
            [['Sumario'], 'string'],
            [['Imagen','ImagenDetalle'], 'file'],
            [['Titulo', 'Seccion'], 'string', 'max' => 250],
            [['Clave'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTema' => 'Id Tema',
            'Fecha' => 'Fecha',
            'Imagen' => 'Imagen Portada',
            'ImagenDetalle' => 'Imagen Detalle',
            'Titulo' => 'Titulo',
            'Sumario' => 'Sumario',
            'Seccion' => 'Seccion',
            'Clave' => 'Palabra clave o frase para busqueda en Watson',
        ];
    }
}
