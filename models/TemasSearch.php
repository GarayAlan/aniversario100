<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Temas;

/**
 * TemasSearch represents the model behind the search form about `app\models\Temas`.
 */
class TemasSearch extends Temas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTema'], 'integer'],
            [['Fecha', 'Imagen', 'Titulo', 'Sumario', 'Seccion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Temas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idTema' => $this->idTema,
            'Fecha' => $this->Fecha,
        ]);

        $query->andFilterWhere(['like', 'Imagen', $this->Imagen])
            ->andFilterWhere(['like', 'Titulo', $this->Titulo])
            ->andFilterWhere(['like', 'Sumario', $this->Sumario])
            ->andFilterWhere(['like', 'Seccion', $this->Seccion]);

        return $dataProvider;
    }
}
