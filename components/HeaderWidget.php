<?php
    namespace app\components;
    use yii\base\Widget;
    use app\models\Temas;
    
    class HeaderWidget extends Widget{
        public $seccion;
        public $nota;
        
        public function init(){
            parent::init();
            $sql = 'SELECT Seccion FROM temas GROUP BY Seccion ORDER BY Seccion';
            $secciones = Temas::findBySql($sql)->all();
            echo $this->render('header',['seccion'=>  $this->seccion,'nota'=>$this->nota,'secciones'=>$secciones]);
        }
    }