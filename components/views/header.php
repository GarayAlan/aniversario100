<?php use app\helpers\String; ?>
<header class="<?php (isset($nota['Anterior'])) ? "" : "home";?>">
<?php if (isset($nota['Anterior'])): ?>
        <div class="prev nav">
			<a href="" class="ct arrow" id="<?= $nota['Anterior']; ?>">
                <div class="tema">
                    <div class="thumb">
                        <img alt="" src="<?= Yii::$app->homeUrl . "images/temas/" . $nota['AntImg']; ?>">
                    </div>
                    <span><?= $nota['AntAnio']; ?></span>
                    <p><?= $nota['AntTit']; ?></p>
                </div>
            </a>
		</div>
<?php else: ?>
        <div class="prev nav">
            <a href="http://aniversario100.eluniversal.com.mx/"></a>
        </div>
<?php endif; ?>
    <div class="secciones">
        <span>100 años de</span>
        <label class="select">
            <select name="" id="comboHead">
                <option value="historias">Historias</option>
                <?php if ($secciones): ?>
                    <?php foreach ($secciones as $sc): ?>
                        <?php if (isset($nota['Seccion'])): ?>
                            <option <?= (String::removerAcentos($nota['Seccion'], 1) == String::removerAcentos($sc->Seccion, 1) ? 'selected' : ''); ?> value="<?= $sc->Seccion; ?>"><?= $sc->Seccion; ?></option>
                        <?php else: ?>
                            <option <?= (String::removerAcentos($seccion, 1) == String::removerAcentos($sc->Seccion, 1) ? 'selected' : ''); ?> value="<?= $sc->Seccion; ?>"><?= $sc->Seccion; ?></option>
                        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>

            </select>
        </label>
        <span>por El Universal</span>
    </div>

<?php if (isset($nota['Siguiente'])): ?>
        
    <div class="next nav">
            <a href="" class="ct arrow" id="<?= $nota['Siguiente']; ?>">
                <div class="tema">
                    <div class="thumb">
                        <img alt="" src="<?= Yii::$app->homeUrl . "images/temas/" . $nota['SigImg']; ?>">
                    </div>
                    <span><?= $nota['SigAnio']; ?></span>
                    <p><?= $nota['SigTit']; ?></p>
                </div>
            </a>
        </div>
<?php else: ?>
        <div class="next nav">
            <a href=""></a>
        </div>
<?php endif; ?>
</header>