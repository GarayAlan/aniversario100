$(function() {
  $('ul.nav a').bind('click',function(event){
    var $anchor = $(this);
    $('.linea-tiempo').stop().animate({
      scrollLeft: $($anchor.attr('href')).offset().left
    }, 2000);
    event.preventDefault();
  });
});