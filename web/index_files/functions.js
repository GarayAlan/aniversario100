$(document).ready(function () {
    cargaHeader(0);
    cargaFooter(1);
    cargaPortada();

    $('body').on('click', '.tema_af', function () {
        cargaHeader($(this).attr('id'));
        cargaTema($(this).attr('id'));
        cargaFooter(2);
        return false;
    });
    
    $('body').on('click', 'footer ul li a', function () {
        $('footer ul li a').removeClass('active');
        $(this).addClass('active');
    });
    
    $('body').on('click', '.tpersonajes', function () {
        cargaHeader($(this).attr('data-id'));
        cargaDetalle($(this).attr('data-id'),'personajes');
        cargaFooter(2);
        return false;
    });
    
    $('body').on('click', '.thechos', function () {
        cargaHeader($(this).attr('data-id'));
        cargaDetalle($(this).attr('data-id'),'hechos');
        cargaFooter(2);
        return false;
    });
    
    $('body').on('click', '.tlugares', function () {
        cargaHeader($(this).attr('data-id'));
        cargaDetalle($(this).attr('data-id'),'lugares');
        cargaFooter(2);
        return false;
    });
    
    $('body').on('click', '.tsentimientos', function () {
        cargaHeader($(this).attr('data-id'));
        cargaDetalle($(this).attr('data-id'),'sentimientos');
        cargaFooter(2);
        return false;
    });
    
    $('body').on('click', '.ttematica', function () {
        cargaHeader($(this).attr('data-id'));
        cargaDetalle($(this).attr('data-id'),'tematica');
        cargaFooter(2);
        return false;
    });
    
    $('body').on('click', '.tfechas', function () {
        cargaHeader($(this).attr('data-id'));
        cargaDetalle($(this).attr('data-id'),'fechas');
        cargaFooter(2);
        return false;
    });

    $('body').on('click', '.rethome', function () {
        cargaHeader(0);
        cargaFooter(1);
        cargaPortada();
        return false;
    });

    $('body').on('click', '.ct', function () {
        cargaHeader($(this).attr('id'));
        cargaTema($(this).attr('id'));
        cargaFooter(2);
        return false;
    });

    $('body').on('change', '#comboHead', function () {
        if ($(this).val() == 'historias'){
            cargaHeader(0);
            cargaFooter(1);
            cargaPortada();
            return false;
        }
        else {
            cargaHeader(0,$(this).val());
            cargaFooter(1);
            cargaHistoria($(this).val());
            return false;
        }
    });
    
    $('body').on('click', '.recorte', function () {
        $('.detalle-hemeroteca').hide();
        $('#hm_'+$(this).attr('data-id')).show();
        return false;
    });
    
    $('body').on('click', '.back', function () {
        $('.detalle-hemeroteca').hide();
        return false;
    });
});

function cargaFooter(tipo) {
    $.ajax({
        url: "footer/" + tipo
    }).done(function (data) {
        $("#f_a").html(data);
    });
}

function cargaHeader(nota,seccion) {
    $.ajax({
        url: "header/" + nota+"/"+seccion,
    }).done(function (data) {
        $("#h_a").html(data);
    });
}

function cargaPortada() {
    $(document.body).removeAttr('class').attr('class', '');
    $.ajax({
        url: "portada"
    }).done(function (data) {
        $("#c_a").html(data);
    });
}

function cargaHistoria(seccion) {
    $(document.body).removeAttr('class').attr('class', '');
    $.ajax({
        url: "historia/" + seccion
    }).done(function (data) {
        $("#c_a").html(data);
    });
}

function cargaTema(tema) {
    $(document.body).removeAttr('class').attr('class', '');
    $.ajax({
        url: "tema/" + tema
    }).done(function (data) {
        $("#c_a").html(data);
    });
}

function cargaDetalle(id,tab){
    $.ajax({
        url: "detalle/" + id+"/"+tab
    }).done(function (data) {
        $("#c_a").html(data);
    });
}
    