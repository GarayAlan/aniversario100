<?php
    namespace app\controllers;
    use Yii;
    use yii\web\Controller;
    use app\models\Temas;

    class CargaController extends Controller {
        public $layout = 'main_c';
        
        public function actionIndex() {
            return $this->render('index');
        }
        
        public function actionTemas() {
            $model = new Temas();
            if($model->load(Yii::$app->request->post())){
                //print_r($model);
                print_r($model->getErrors());
            } else {
                return $this->render('temas', ['model' => $model]);
            }
        }
    }