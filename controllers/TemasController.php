<?php

namespace app\controllers;

use Yii;
use app\models\Temas;
use app\models\Recortes;
use app\models\TemasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Personajes;
use app\helpers\String;


/**
 * TemasController implements the CRUD actions for Temas model.
 */
class TemasController extends Controller
{
    public $layout = 'main_c';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Temas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TemasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Temas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionElimina($id){
        ini_set('max_execution_time', "0");
        ini_set("memory_limit", "-1");
        $cache = Yii::$app->cache;
        $cache->flush('tema_'.$id);
        $cache->flush('fca_'.$id);
        $cache->flush('total_'.$id);
        $cache->flush('cobertura_'.$id);
        $cache->flush('coberturamayor_'.$id);
        $cache->flush('personajes_'.$id);
        $cache->flush('personajeswt_'.$id);
        $cache->flush('lugares_'.$id);
        $cache->flush('hemeroteca_'.$id);
        
        
        $cache->flush('cr_'.$id);
        $cache->flush('cf_'.$id);
        $cache->flush('ca_'.$id);
        $cache->flush('ct_'.$id);
        $cache->flush('cl_'.$id);
        $cache->flush('cp_'.$id);
        $cache->flush('cs_'.$id);
        echo "Se elimino la cache del tema ".$id;
    }
    
    public function actionCache($id){
        ini_set('max_execution_time', "0");
        ini_set("memory_limit", "-1");
        header('Content-Type: text/html; charset=UTF-8');
        
        $cache = Yii::$app->cache;
        $tema  = Temas::find()->where(['idTema'=>$id])->one();
        
        $cache_tema = $cache->get('tema_'.$id);
        if ($cache_tema === false) {
            $cache->set('tema_'.$id, $tema,0);
        }
        
        $arrayanios = [];
        $cache_anios = $cache->get('fca_'.$id);
        if ($cache_anios === false){
            $urlanio = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"anio"}';
            $anios = json_decode($this->curlSendGet($urlanio));
            
            if(is_array($anios->es_apiResponse->ibmsc_facet->ibmsc_facetValue)){
                foreach($anios->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $resultados){
                    $arrayanios[$resultados->label] = $resultados->weight;
                }
            }
            else{
                $arrayanios[$anios->es_apiResponse->ibmsc_facet->ibmsc_facetValue->label] = $anios->es_apiResponse->ibmsc_facet->ibmsc_facetValue->weight;
            }
            ksort($arrayanios);
            $cache->set('fca_'.$id, $arrayanios,0);
            $cache->set('total_'.$id, $anios->es_apiResponse->es_totalResults,0);
        }
        
        $arraycobertura = [];
        $cache_cobertura = $cache->get('cobertura_'.$id);
        if ($cache_cobertura === false){
            $urlcobertura = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"SECCION"}';
            $secciones = json_decode($this->curlSendGet($urlcobertura));
            if(is_array($secciones->es_apiResponse->ibmsc_facet->ibmsc_facetValue)){
                $c = 0;
                $resto = 0;
                $count = count($secciones->es_apiResponse->ibmsc_facet->ibmsc_facetValue);
                
                foreach($secciones->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $resultados){
                    if($c==0){
                        $cache->set('coberturamayor_'.$id, String::removerAcentos2($resultados->label),0);
                    }
                    if($c <= 4){
                        $arraycobertura[String::removerAcentos2($resultados->label)] = $resultados->weight;
                    }
                    else{
                        $resto += $resultados->weight;
                    }
                    $c++;
                }
                if($count > 4){
                    $arraycobertura['Resto'] = $resto;
                }
            }
            else{
                $arraycobertura[String::removerAcentos2($secciones->es_apiResponse->ibmsc_facet->ibmsc_facetValue->label)] = $secciones->es_apiResponse->ibmsc_facet->ibmsc_facetValue->weight;
            }
            $cache->set('cobertura_'.$id, $arraycobertura,0);
        }
        
        $arraypersona = [];
        $cache_personajes = $cache->get('personajes_'.$id);
        if ($cache_personajes === false){
            $personas = Personajes::find()->where(['idTema'=>$id])->all();
            foreach($personas as $pers){
                $aniper = [];
                $url = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($pers->nombre).'","facet":"anio"}';
                $res = json_decode($this->curlSendGet($url));
                if(is_array($res->es_apiResponse->ibmsc_facet->ibmsc_facetValue)){
                    foreach($res->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $resultados){
                        $aniper[$resultados->label] = $resultados->weight;
                    }
                }
                else{
                    $aniper[$res->es_apiResponse->ibmsc_facet->ibmsc_facetValue->label] = $res->es_apiResponse->ibmsc_facet->ibmsc_facetValue->weight;
                }
                ksort($aniper);
                $arraypersona[$pers->idPersona] = [
                    'nombre'     => $pers->nombre,
                    'imagen'     => $pers->imagen,
                    'resultados' => $res->es_apiResponse->es_totalResults,
                    'anios'      => $aniper
                ];
            }
            
            $cache->set('personajes_'.$id, $arraypersona,0);
        }
        
        $arraylugares = [];
        $cache_lugares = $cache->get('lugares_'.$id);
        if ($cache_lugares === false){
            $urllugares = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"LUGAR"}';
            $lugares = json_decode($this->curlSendGet($urllugares));
            
            if(is_array($lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue)){
                foreach($lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $resultados){
                    $arraylugares[String::removerAcentos2($resultados->label)] = $resultados->weight;
                }
            }
            else{
                $arraylugares[String::removerAcentos2($lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue->label)] = $lugares->es_apiResponse->ibmsc_facet->ibmsc_facetValue->weight;
            }
            ksort($arraylugares);
            $cache->set('lugares_'.$id, $arraylugares,0);
        }
        
        $arraypwt = [];
        $cache_pwt = $cache->get('personajeswt_'.$id);
        if ($cache_pwt === false){
            $urlper = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"PERSONAS"}';
            $perwt = json_decode($this->curlSendGet($urlper));
            
            if(is_array($perwt->es_apiResponse->ibmsc_facet->ibmsc_facetValue)){
                foreach($perwt->es_apiResponse->ibmsc_facet->ibmsc_facetValue as $resultados){
                    $arraypwt[String::removerAcentos2($resultados->label)] = $resultados->weight;
                }
            }
            else{
                $arraypwt[String::removerAcentos2($perwt->es_apiResponse->ibmsc_facet->ibmsc_facetValue->label)] = $perwt->es_apiResponse->ibmsc_facet->ibmsc_facetValue->weight;
            }
            //ksort($arraypwt);
            $cache->set('personajeswt_'.$id, $arraypwt,0);
        }
        
        $cache_hemeroteca = $cache->get('hemeroteca_'.$id);
        if ($cache_hemeroteca === false){
            $recortes = Recortes::find()->where(['idTema'=>$id])->all();
            $cache->set('hemeroteca_'.$id, $recortes,0);
        }
        
        die();
        
        
        
        
        $cache_resultado = $cache->get('cr_'.$id);
        if ($cache_resultado === false) {
            $url  = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'"}';
            $cache->set('cr_'.$id, json_decode($this->curlSendGet($url)),0);
        }
        
        

        $cache_temas = $cache->get('ct_'.$id);
        if ($cache_temas === false) {
            $urltemati = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"SECCION"}';
            $cache->set('ct_'.$id, json_decode($this->curlSendGet($urltemati)),0);
        }

        $cache_lugares = $cache->get('cl_'.$id);
        if ($cache_lugares === false) {
            $urllugare = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"LUGAR"}';
            $cache->set('cl_'.$id, json_decode($this->curlSendGet($urllugare)),0);
        }

        $cache_personas = $cache->get('cp_'.$id);
        if ($cache_personas === false) {
            $urlperson = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"PERSONAS"}';
            $cache->set('cp_'.$id, json_decode($this->curlSendGet($urlperson)),0);
        }

        $cache_sentimientos = $cache->get('cs_'.$id);
        if ($cache_sentimientos === false) {
            $urlsentim = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"SENTIMIENTO"}';
            $cache->set('cs_'.$id, json_decode($this->curlSendGet($urlsentim)),0);
        }
        
        print_r($cache->get('ct_'.$id));
        
    }
    
    
    public function actionCarga(){
        ini_set('max_execution_time', "0");
        ini_set("memory_limit", "-1");
        $temas = Temas::find()->all();
        if($temas){
            foreach($temas as $tema){
                $id = $tema->idTema;
                $this->actionCache($id);
            }
        }
        
    }
    
    /**
     * Informacion de Watson
     * @param integer $id
     * @return mixed
     */
    public function actionWatson($id)
    {
        ini_set('max_execution_time', "0");
        ini_set("memory_limit", "-1");
        $tema = Temas::find()->where(['idTema'=>$id])->one();
        $cachet = 'NO';
        $cache = Yii::$app->cache;
        $cache_resultado = $cache->get('cr_'.$id);
        if ($cache_resultado === false) {
            $cachet = 'SI';
            $url  = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'"}';
            $cache->set('cr_'.$id, json_decode($this->curlSendGet($url)),0);
            $cache->set('urlcr_'.$id, $url,0);
        }
        
        $cache_anio = $cache->get('ca_'.$id);
        if ($cache_anio === false) {
            $cachet  = 'SI';
            $urlanio = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"anio"}';
            $cache->set('ca_'.$id, json_decode($this->curlSendGet($urlanio)),0);
            $cache->set('urlca_'.$id, $urlanio,0);
        }
        
        $cache_fechas = $cache->get('cf_'.$id);
        if ($cache_fechas === false) {
            $urlfechas = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"FECHA"}';
            $cache->set('cf_'.$id, json_decode($this->curlSendGet($urlfechas)),0);
            $cache->set('urlcf_'.$id, $urlfechas,0);
            $cachet = 'SI';
        }
        
        
        
        $cache_temas = $cache->get('ct_'.$id);
        if ($cache_temas === false) {
            $urltemati = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"SECCION"}';
            $cache->set('ct_'.$id, json_decode($this->curlSendGet($urltemati)),0);
            $cache->set('urlct_'.$id, $urltemati,0);
            $cachet= 'SI';
        }
        
        
        
        $cache_lugares = $cache->get('cl_'.$id);
        if ($cache_lugares === false) {
            $urllugare = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"LUGAR"}';
            $cache->set('cl_'.$id, json_decode($this->curlSendGet($urllugare)),0);
            $cache->set('urlcl_'.$id, $urllugare,0);
            $cachet = 'SI';
        }
        
        $cache_personas = $cache->get('cp_'.$id);
        if ($cache_personas === false) {
            $urlperson = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"PERSONAS"}';
            $cache->set('cp_'.$id, json_decode($this->curlSendGet($urlperson)),0);
            $cache->set('urlcp_'.$id, $urlperson,0);
            $cachet = 'SI';
        }
        
        $cache_sentimientos = $cache->get('cs_'.$id);
        if ($cache_sentimientos === false) {
            $urlsentim = 'http://172.16.50.200:8080/universal-facets/watson/search?jsonSearch={"search":"'.urlencode($tema->Clave).'","facet":"SENTIMIENTO"}';
            $cache->set('cs_'.$id, json_decode($this->curlSendGet($urlsentim)),0);
            $cache->set('urlcs_'.$id, $urlsentim,0);
            $cachet = 'SI';
        }
        
        return $this->render('watson', [
            'global'       => $cache->get('cr_'.$id),
            'documentos'   => @$cache->get('cr_'.$id)->es_apiResponse->es_result,
            'sentimientos' => $cache->get('cs_'.$id),
            'personajes'   => $cache->get('cp_'.$id),
            'lugares'      => $cache->get('cl_'.$id),
            'tematica'     => $cache->get('ct_'.$id),
            'fechas'       => $cache->get('cf_'.$id),
            'anio'         => $cache->get('ca_'.$id),
            'clave'        => $tema->Clave,
            'urlg'         => $cache->get('urlcr_'.$id), 
            'urlff'        => $cache->get('urlcf_'.$id), 
            'urlft'        => $cache->get('urlct_'.$id), 
            'urlfl'        => $cache->get('urlcl_'.$id), 
            'urlfp'        => $cache->get('urlcp_'.$id), 
            'urlfs'        => $cache->get('urlcs_'.$id),
            'urlan'        => $cache->get('urlca_'.$id),
        ]);
    }
    
    /**
     * Informacion de Watson
     * @param integer $id
     * @return mixed
     */
    public function actionRecortes($id)
    {
        $recortes = Recortes::find()->where(['idTema'=>$id])->all();
        return $this->render('recortes', ['recortes'=>$recortes]);
    }

    /**
     * Creates a new Temas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Temas();

        if ($model->load(Yii::$app->request->post())) {
            $model->Imagen = UploadedFile::getInstance($model, 'Imagen');
            $model->ImagenDetalle = UploadedFile::getInstance($model, 'ImagenDetalle');
            if ($model->save()) {                
                $model->Imagen->saveAs('images/temas/' . $model->Imagen->baseName . '.' . $model->Imagen->extension);
                $model->ImagenDetalle->saveAs('images/temas/' . $model->ImagenDetalle->baseName . '.' . $model->ImagenDetalle->extension);
                $cache = Yii::$app->cache;
                //$cache->flush();
                return $this->redirect(['view', 'id' => $model->idTema]);
            }
            else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Temas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $archivo1 = UploadedFile::getInstance($model, 'Imagen');
            if($archivo1){
                if ($archivo1->size !== 0)
                    $model->Imagen = $archivo1;
                else
                    $model->Imagen = Temas::findone($model->idTema)->Imagen;
            }
            else{
                $model->Imagen = Temas::findone($model->idTema)->Imagen;
            }
            
            
            $archivo2 = UploadedFile::getInstance($model, 'ImagenDetalle');
            if($archivo2){
                if ($archivo2->size !== 0)
                    $model->ImagenDetalle = $archivo2;
                else
                    $model->ImagenDetalle = Temas::findone($model->idTema)->ImagenDetalle;
            }
            else{
                $model->ImagenDetalle = Temas::findone($model->idTema)->ImagenDetalle;
            }
            
            //$model->Imagen = UploadedFile::getInstance($model, 'Imagen');
            //$model->ImagenDetalle = UploadedFile::getInstance($model, 'ImagenDetalle');
            if ($model->save()) {  
                if($archivo1){
                    if ($archivo1->size !== 0){
                        $archivo1->saveAs('images/temas/' . $model->Imagen->baseName . '.' . $model->Imagen->extension);
                    }
                }
                if($archivo2){
                    if ($archivo2->size !== 0){
                        $archivo2->saveAs('images/temas/' . $model->ImagenDetalle->baseName . '.' . $model->ImagenDetalle->extension);
                    }
                }
                //$model->Imagen->saveAs('images/temas/' . $model->Imagen->baseName . '.' . $model->Imagen->extension);
                //$model->ImagenDetalle->saveAs('images/temas/' . $model->ImagenDetalle->baseName . '.' . $model->ImagenDetalle->extension);
                
                $cache = Yii::$app->cache;
                //$cache->flush();
                return $this->redirect(['view', 'id' => $model->idTema]);
            }
            else {
            return $this->render('create', [
                'model' => $model,
            ]);}
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Temas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $cache = Yii::$app->cache;
        //$cache->flush();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Temas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Temas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Temas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function curlSendGet($url){
            $vCurl = curl_init($url);
            curl_setopt($vCurl, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($vCurl, CURLOPT_RETURNTRANSFER, true);
            $vRes = curl_exec($vCurl);
            curl_close($vCurl);
            return $vRes;
        }
        
    private function curlSendPostJSON($url, $data) {
        $vCurl = curl_init($url);
        curl_setopt($vCurl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($vCurl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($vCurl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($vCurl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data)]
        );

        $vRes = curl_exec($vCurl);
        curl_close($vCurl);
        return $vRes;
    }
}
