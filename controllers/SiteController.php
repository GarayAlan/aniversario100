<?php
    namespace app\controllers;
    use Yii;
    use yii\web\Controller;
    use app\models\Temas;
    use app\models\Recortes;

    class SiteController extends Controller {
        public $layout = 'main_ra';
        
        /**
         * Home
         */
        public function actionIndex(){
            return $this->render('index');
        }
        
        /**
         * Header
         */
        public function actionHeader($id,$seccion){
            $cache = Yii::$app->cache;
            $cachea = $cache->get('header_'.$id);
            if ($cachea === false){
                $tema = Temas::findOne(['idTema'=>$id]);
                $siguiente = Temas::find()->where(['>','Fecha' ,@$tema->Fecha])->orderBy(['Fecha'=>SORT_ASC])->limit(1)->all();
                $anterior = Temas::find()->where(['<','Fecha' ,@$tema->Fecha])->orderBy(['Fecha'=>SORT_DESC])->limit(1)->all();
                $arregloHD = [
                    'Seccion' =>@$tema->Seccion,
                    'Anterior' =>@$anterior[0]->idTema,
                    'Siguiente' =>@$siguiente[0]->idTema,
                    'AntImg' =>@$anterior[0]->Imagen,
                    'AntTit' =>@$anterior[0]->Titulo,
                    'SigImg' =>@$siguiente[0]->Imagen,
                    'SigTit' =>@$siguiente[0]->Titulo,
                    'SigAnio' =>date('Y',  strtotime(@$siguiente[0]->Fecha)),
                    'AntAnio' =>date('Y',  strtotime(@$anterior[0]->Fecha)),
                    'Id' =>$id
                ];
                $cache->set('header_'.$id, $arregloHD,0);
            }
            return $this->renderPartial('header',['sec_def'=>$seccion,'nota'=>($cache->get('header_'.$id))]);
        }
        
        public function actionView($id){
            Yii::$app->view->params['bclass'] = 'interior detalle';
            $cache = Yii::$app->cache;
            $cachea = $cache->get('cd_'.$id);
            if ($cachea === false) {
                $info = $this->curlSendGet('http://api.eluniversal.com.mx/v1/note/json/eluniversal/'.$id);     
                $cache->set('cd_'.$id, json_decode($info),0);
            }
            return $this->render('detalle',['nota'=>($cache->get('cd_'.$id))]);
        }
        
        public function actionEstadisticas($id,$ficha){
            Yii::$app->view->params['bclass'] = 'interior estadisticas';
            $info = $this->curlSendGet('http://api.eluniversal.com.mx/v1/note/json/eluniversal/'.$id);     
            return $this->render('estadisticas',['nota'=>json_decode($info)]);
        }
        
        public function actionFlush() {
            $cache = Yii::$app->cache;
            $cache->flush();
        }
        
        private function curlSendGet($url){
            $vCurl = curl_init($url);
            curl_setopt($vCurl, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($vCurl, CURLOPT_RETURNTRANSFER, true);
            $vRes = curl_exec($vCurl);
            curl_close($vCurl);
            return $vRes;
        }
        
        
        
        public function actionFooter($id){
            if($id > 1 ){
                return $this->renderPartial('footer2');
            }
            else{
                return $this->renderPartial('footer');
            }
        }
        
        
        
        
        
        public function actionHistoria($seccion){
            $cache = Yii::$app->cache;
            $cacheportada = $cache->get('portada_'.$seccion);
            if ($cacheportada === false) {
                $arreglote = [];
                $temas = Temas::find()->where(['Seccion'=>$seccion])->orderBy(['Fecha'=>SORT_ASC])->all();
                foreach($temas as $tema){
                    $anio = date('Y',  strtotime($tema->Fecha));
                    $decada = floor($anio / 10) * 10;
                    $arreglote[$decada][] = [
                        'Titulo' => $tema->Titulo,
                        'Imagen' => $tema->Imagen,
                        'Fecha'  => $tema->Fecha,
                        'Sumario' => $tema->Sumario,
                        'Seccion' => $tema->Seccion,
                        'IdTema'  => $tema->idTema
                    ];
                }
                $cache->set('portada_'.$seccion, $arreglote,0);
            }
            return $this->renderPartial('sechis',['notas_f'=>$cache->get('portada_'.$seccion),'seccion'=>$seccion]);
        }
        
        
        
        public function actionDetallado($id,$tab){
            $cache = Yii::$app->cache;
            $cachea = $cache->get('tema_'.$id);
            if ($cachea === false) {
                $cache->set('tema_'.$id, Temas::findOne(['idTema'=>$id]),0);
            }
            $n = 0;
            switch ($tab){
                case 'fechas':     $n = 0; break;
                case 'personajes': $n = 1; break;
                case 'lugares':    $n = 2; break;
                case 'hechos':     $n = 3; break;
            }
            
            return $this->renderAjax('tabs',[
                'cobertura'      => $cache->get('cobertura_'.$id),
                'coberturamayor' => $cache->get('coberturamayor_'.$id),
                'personajes'     => $cache->get('personajes_'.$id),
                'personajeswt'   => $cache->get('personajeswt_'.$id),
                'lugares'        => $cache->get('lugares_'.$id),
                'hemeroteca'     => $cache->get('hemeroteca_'.$id),
                'n_tab'          => $n,
                'active'         => $tab,
                'id'             => $id,
                'nota'           => $cache->get('tema_'.$id)
            ]);
        }
        
        
        
        /**********************************/
        /** Funciones pulidas y probadas **/
        /**********************************/
        public function actionPortada(){
            $cache = Yii::$app->cache;
            $cacheportada = $cache->get('portada_ajax');
            if ($cacheportada === false) {
                $arreglote = [];
                $temas = Temas::find()->orderBy(['Fecha'=>SORT_ASC])->all();
                foreach($temas as $tema){
                    $anio = date('Y',  strtotime($tema->Fecha));
                    $decada = floor($anio / 10) * 10;
                    $arreglote[$decada][] = [
                        'Titulo' => $tema->Titulo,
                        'Imagen' => $tema->Imagen,
                        'Fecha'  => $tema->Fecha,
                        'Sumario' => $tema->Sumario,
                        'Seccion' => $tema->Seccion,
                        'IdTema'  => $tema->idTema
                    ];
                }
                $cache->set('portada_ajax', $arreglote,0);
            }
            return $this->renderPartial('portada',['notas_f'=>$cache->get('portada_ajax')]);
        }
        
        public function actionTema($id){
            $cache = Yii::$app->cache;
            return $this->renderAjax('detalle',[
                'nota'   =>($cache->get('tema_'.$id)),
                'global' => $cache->get('cr_'.$id),
                'cuenta' => $cache->get('fca_'.$id),
                'total'  => $cache->get('total_'.$id)
            ]);
        }
        
        
        
        
    }