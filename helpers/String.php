<?php
    namespace app\helpers;
        
    class String
    {
        /**
         * Funcion para remover espacios y acentos en una cadena
         * @param string $cadena
         * @param boolean $comscore 
         * return string
         */
        public static function removerAcentos($cadena,$comscore = 0)
        {
            $s = strtolower(htmlentities($cadena));
            
            $s = str_replace("&ntilde;","n",$s);
            $s = str_replace("&aacute;","a",$s);
            $s = str_replace("&eacute;","e",$s);
            $s = str_replace("&iacute;","i",$s);
            $s = str_replace("&oacute;","o",$s);
            $s = str_replace("&uacute;","u",$s);
            if($comscore == 1){
                $s = str_replace(' ','-',$s);
                $s = str_replace(',','',$s);
                $s = str_replace('.','',$s);
                $s = str_replace(':','',$s);
                $s = str_replace(';','',$s);
            }else{
                $s = str_replace(' ','',$s);
            }
            return $s;
        }
        
        /**
         * Funcion para remover espacios y acentos en una cadena
         * @param string $cadena
         * @param boolean $comscore 
         * return string
         */
        public static function removerAcentos2($cadena)
        {
            $s = ($cadena);
            
            $s = str_replace("Ã©","E",$s);
            $s = str_replace("Ã±","N",$s);
            $s = str_replace("Ã¡","A",$s);
            $s = str_replace("Ã³","O",$s);
            $s = str_replace("Ãº","U",$s);
            $s = str_replace("Ã­","I",$s);
            $s = str_replace("Ã","E",$s);
            
            $s = strtoupper($s);
            
            switch($s){
                case 'ESTADOS UNIDOS': $s = 'UNITED STATES';break;
                case 'ESPANA':         $s = 'SPAIN';break;
                case 'ALEMANIA':       $s = 'GERMANY';break;
                case 'BRASIL':         $s = 'BRAZIL';break;
                case 'FRANCIA':        $s = 'FRANCE';break;
                case 'ITALIA':         $s = 'ITALY';break;
                case 'RUSIA':          $s = 'RUSSIA';break;
                case 'SUIZA':          $s = 'SWITZERLAND';break;
                case 'BELGICA':        $s = 'BELGIUM';break;
                case 'POLONIA':        $s = 'POLAND';break;
                case 'DINAMARCA':      $s = 'DENMARK';break;
                case 'EGIPTO':         $s = 'EGYPT';break;
                case 'GRECIA':         $s = 'GREECE';break;
                case 'IRLANDA':        $s = 'IRELAND';break;
                case 'FILIPINAS':      $s = 'PHILIPPINES';break;
                case 'NORUEGA':        $s = 'NORWAY';break;
                case 'REINO UNIDO':    $s = 'GB';break;
                case 'COREA DEL SUR':  $s = 'SOUTH KOREA';break;
                case 'MARRUECOS':      $s = 'MOROCCO';break;
                case 'JAPON':          $s = 'JAPAN';break;
                case 'HUNGRIA':          $s = 'HUNGARY';break;
                case 'ETIOPIA':          $s = 'ETHIOPIA';break;
                case 'TURQUIA':          $s = 'TURKEY';break;
                default: $s = $s;
            }
                       
            
            return $s;
        }
    }