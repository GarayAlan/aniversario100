<?php
    namespace app\helpers;
    
    class Fechas
    {
        /**
         * Convertir Fecha MYSQL YYYY-MM-AA a formato de lectura humano
         */
        public static function formato($fecha){
            $meses = [
                '01' => 'Enero','02' => 'Febrero','03' => 'Marzo','04' => 'Abril','05' => 'Mayo','06' => 'Junio','07' => 'Julio',
                '08' => 'Agosto','09' => 'Septiembre','10' => 'Octubre','11' => 'Noviembre','12' => 'Diciembre'
            ];
                        
            $a_f = substr($fecha,0,4);
            $m_f = substr($fecha,5,2);
            $d_f = substr($fecha,8,2);
            return $d_f." ".$meses[$m_f]." ".$a_f;
        }
    }