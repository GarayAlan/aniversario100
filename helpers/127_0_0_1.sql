-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-09-2016 a las 15:11:29
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `aniversario100`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temas`
--

CREATE TABLE IF NOT EXISTS `temas` (
  `idTema` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Imagen` varchar(500) DEFAULT NULL,
  `Titulo` varchar(250) NOT NULL,
  `Sumario` text NOT NULL,
  `Seccion` varchar(250) NOT NULL,
  `ImagenDetalle` varchar(500) NOT NULL,
  `Clave` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idTema`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `temas`
--

INSERT INTO `temas` (`idTema`, `Fecha`, `Imagen`, `Titulo`, `Sumario`, `Seccion`, `ImagenDetalle`, `Clave`) VALUES
(1, '1916-10-01', 'portada_tema1.png', 'Primera portada del periódico El Universal', 'Primera portada del periódico El Universal - Aparece la primer portada de El Universal - sumario asdf', 'El Universal', 'detalle_tema1.jpg', 'Funerales de Emiliano Zapata'),
(3, '1917-02-02', 'portada_tema2.jpg', 'Firma de la Constitución de 1917', 'Firma de la Constitución de 1917 - Sumario', 'Nacional', 'detalle_tema2.jpg', 'Firma de la Constitución de 1917'),
(4, '1918-04-10', 'mexico.jpg', 'La Ciudad de México vuelve a ser capital de México', 'La Ciudad de México vuelve a ser capital de México - Sumario cambiado', 'Nacional', 'detalle_tema3.jpg', 'La Ciudad de México vuelve a ser capital de México'),
(5, '1970-06-01', 'portada_tema_d.png', 'Copa Mundial de Fútbol de 1970', 'Copa del Mundo de 1970', 'Deportes', 'detalle_tema_d.jpg', 'Copa Mundial de Fútbol de 1970');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
