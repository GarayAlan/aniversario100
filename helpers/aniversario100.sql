-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-09-2016 a las 16:38:19
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `aniversario100`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personajes`
--

CREATE TABLE IF NOT EXISTS `personajes` (
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `idTema` int(11) NOT NULL,
  `nombre` varchar(500) DEFAULT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idPersona`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `personajes`
--

INSERT INTO `personajes` (`idPersona`, `idTema`, `nombre`, `imagen`) VALUES
(1, 1, 'Félix Fulgencio Palavicini', 'palavicini1.jpg'),
(3, 1, 'Juan Francisco Ealy Ortiz', 'ealy1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recortes`
--

CREATE TABLE IF NOT EXISTS `recortes` (
  `idRecorte` int(11) NOT NULL AUTO_INCREMENT,
  `idTema` int(11) NOT NULL,
  `nombre` varchar(1024) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `seccion` varchar(500) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`idRecorte`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recortes`
--

INSERT INTO `recortes` (`idRecorte`, `idTema`, `nombre`, `imagen`, `seccion`, `fecha`, `text`) VALUES
(1, 1, 'TITULO 1', 'recorte1.jpg', 'Ciudad', '2016-09-01', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet vulputate nisl, ac lacinia arcu. Maecenas aliquet imperdiet eros. Nam condimentum placerat enim, non tempor felis ullamcorper sed. Sed at dictum sem. Aenean eget massa et neque laoreet ornare nec a ante. Phasellus vel commodo nulla. Nullam felis felis, pulvinar vitae dolor eu, accumsan scelerisque neque. Morbi nec quam ut erat dictum hendrerit ut vel libero. Phasellus velit neque, tincidunt quis semper ut, congue in quam.'),
(2, 1, 'TITULO 2', 'recorte2.jpg', 'El Universal', '1990-02-02', 'NOT IPSUM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temas`
--

CREATE TABLE IF NOT EXISTS `temas` (
  `idTema` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Imagen` varchar(500) DEFAULT NULL,
  `Titulo` varchar(250) NOT NULL,
  `Sumario` text NOT NULL,
  `Seccion` varchar(250) NOT NULL,
  `ImagenDetalle` varchar(500) NOT NULL,
  `Clave` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idTema`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `temas`
--

INSERT INTO `temas` (`idTema`, `Fecha`, `Imagen`, `Titulo`, `Sumario`, `Seccion`, `ImagenDetalle`, `Clave`) VALUES
(1, '1916-10-01', 'portada_tema1.png', 'Primera portada de El Universal', 'Sumario', 'El Universal', 'tratad-de-versalles-600x310.jpg', 'el universal'),
(2, '2016-08-20', 'universal98_4.jpg', 'Juan Gabriel', 'Juanga', 'Espectáculos', 'mexico.jpg', 'Juan Gabriel'),
(3, '2014-01-20', '92.jpg', 'Tsunami en Japón', 'tsunami', 'Mundo', '92A.jpg', 'Tsunami en japon'),
(4, '1970-08-10', 'portada_tema_d.png', 'Copa Mundial de Fútbol de 1970', 'mundial del 70', 'Deportes', 'detalle_tema_d.jpg', 'Gran Premio de Mexico'),
(5, '1980-10-25', 'portada_tema2.jpg', 'Otro Titulo', 'atentado', 'Nacional', 'erupcion_volcan_chile.jpg', 'Carranza'),
(6, '1990-01-01', '96.jpg', 'Prueba', '2', 'Política', 'lolita.jpg', 'Pacto por Mexico');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
