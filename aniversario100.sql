-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-07-2016 a las 12:54:36
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `aniversario100`
--
CREATE DATABASE IF NOT EXISTS `aniversario100` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `aniversario100`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temas`
--

DROP TABLE IF EXISTS `temas`;
CREATE TABLE IF NOT EXISTS `temas` (
  `idTema` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Imagen` varchar(500) DEFAULT NULL,
  `Titulo` varchar(250) NOT NULL,
  `Sumario` text NOT NULL,
  `Seccion` varchar(250) NOT NULL,
  PRIMARY KEY (`idTema`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `temas`
--

INSERT INTO `temas` (`idTema`, `Fecha`, `Imagen`, `Titulo`, `Sumario`, `Seccion`) VALUES
(1, '1916-10-01', 'universal98_4.jpg', 'Primera portada del periódico El Universal', 'Primera portada del periódico El Universal - Aparece la primer portada de El Universal', 'El Universal'),
(3, '1917-02-02', 'cancilleria.jpg', 'Firma de la Constitución de 1917', 'Firma de la Constitución de 1917 - Sumario', 'Nacional'),
(4, '1918-04-10', 'nochixtlan_redis2.jpg', 'La Ciudad de México vuelve a ser capital de México', 'La Ciudad de México vuelve a ser capital de México - Sumario', 'Nacional'),
(5, '1919-03-14', 'piccccccccninaaaa.jpg', 'Nuevos Billetes del Peso Mexicano', 'Nuevos Billetes del Peso Mexicano -s', 'Política'),
(6, '2016-02-01', 'se-oyeron.jpg', '43 ayotzinapa', '43 ayotzinapa', 'Nacional'),
(7, '1920-01-09', 'erupcion_volcan_chile.jpg', 'Surge un nuevo volcan', 'Sumario del tema, este deberá ser escrito por el personal de carga', 'Nacional'),
(8, '1928-08-26', 'lolita.jpg', 'Primer Viaje de Dolores del Río a Hollywood', 'Cronica del primer viaje de esta señora a Hollywood', 'Espectáculos'),
(9, '1919-06-29', 'tratad-de-versalles-600x310.jpg', 'Firma del Tratado de Versalles', 'Firma del Tratado de Versalles', 'Mundo');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
